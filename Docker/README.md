# Docker

This directory contains the docker containers for easy setup of the project.

## Structure

Every container has its own directory along with a `Dockerfile` if necessary and a `data/` directory which is mounted to the container. It persists the data even if the container is removed.

The directory contains the following containers (folders):

### `mongo/`

This container is used to run the MongoDB database. It is used to store the CVE-Search database.

### `mysql/`

This container is used to run the MySQL database. It is used to store the mapping database.

### `python/`

This container is used along the MongoDB container. It is used to run the CVE-Search tool to populate the MongoDB database.

### `redis/`

The redis container is used along the MongoDB container. It is used as a cache for the CVE-Search tool.

### `commit-analysis/`

This is the main container. It includes an `init_scripts` folder which is run when the container is started.

#### The init_scripts

The init_scripts are run when the container is started.

It includes the following scripts:

- `clone_repos.sh`: This script clones the repositories from GitHub which are used in the project.
- `get_linux_full.sh`: Usually, the GitHub repository is used to get the Linux kernel. However, the repository does not
  include the full history. This script clones the full history of the Linux kernel.
- `get_mappings_all.sh`: Runs all mapping scripts to populate the MySQL database.
- `run_heuristic_all_parallel.sh`: Runs the heuristic on all the entries in the MySQL database. It runs the script in
  parallel to speed up the process.
- `run_all.sh`: This script runs all the other scripts in the correct order.
- `repo_urls.txt`: This file contains the URLs to the repositories which are used in the project. It is used by the
  `clone_repos.sh` script.

> **_NOTE:_** Make sure to look into the `run_all.sh` script if you do not want to run all scripts.
> You can comment out the scripts you do not want to run.