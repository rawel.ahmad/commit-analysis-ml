#!/bin/bash
# Usually, the github repo is used, but for linux it does not contain the full history
# This script is used to get the full history of the Linux kernel
cd /srv/vcc_repos/
wget https://archive.org/download/git-history-of-linux/full-history-linux.git.tar
tar -xvf full-history-linux.git.tar
rm full-history-linux.git.tar
mv history-torvalds linux
chown -R root linux/
cd linux/
git replace --convert-graft-file
git pull