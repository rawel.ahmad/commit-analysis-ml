#!/bin/bash

# 1. Clone the repos
/project/Docker/commit-analysis/init_scripts/clone_repos.sh

# 2. Get the linux full repo (necessary if step 1 is not skipped)
/project/Docker/commit-analysis/init_scripts/get_linux_full.sh

# 3. Get the mappings
/project/Docker/commit-analysis/init_scripts/get_mappings_all.sh

# 4. OSS-Fuzz
# This is a special case, because the OSS-Fuzz token must be set in the environment and updated regularly
# Comment it in after setting the token (see Data/DatasetSources/README.md for instructions)
# python /project/Data/DatasetSources/oss-fuzz/parser.py

# This script parses all fixing commit ranges and extracts one commit per range
# Comment it in if the OSS-Fuzz dataset is already present from step 4
# python /project/Data/DatasetSources/oss-fuzz/processor.py

# Run the cleanup script (Do not comment this out)
python /project/Data/Utils/CleanupUtils.py

# 4. Run the heuristic
/project/Docker/commit-analysis/init_scripts/run_heuristic_all_parallel.sh

# 5. Post-processing
# Associates one commit for each heuristic result
# If it is ambiguous, it will be skipped
python /project/Data/Utils/HeuristicUtils.py

# 6. Run the classifier

# 6.1. Extract the features
python /project/Classifier/main.py