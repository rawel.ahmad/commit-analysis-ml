#!/bin/bash

# This script clones all repositories from the list of URLs in repo_urls.txt
repo_urls="/project/Docker/commit-analysis/init_scripts/repo_urls.txt"

mkdir /srv/vcc_repos -p

while read -r line; do
  echo "Cloning repository $line"
  cd /srv/vcc_repos
  git clone "$line"
done <"$repo_urls"

# the libreoffice repository is cloned as core, so rename it
cd /srv/vcc_repos
mv core libreoffice
# also rename releases-comm-central to thunderbird
mv releases-comm-central thunderbird