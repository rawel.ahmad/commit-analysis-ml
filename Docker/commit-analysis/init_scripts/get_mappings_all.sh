#!/bin/bash
# Runs all the scripts to get the mappings for all the datasets

cd /project/Data/DatasetSources/

# Debian Security Tracker
wget https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/CVE/list -O /project/Data/DatasetSources/DebianSecurityTracker/list
python DebianSecurityTracker/import_debian_security_tracker.py

# Linux Kernel CVEs
python linux_kernel_cves/import_linux_kernel_cves.py

# Piantadosi
python PiantadosiApache/import_httpd_piantadosi.py

# Vulnerability History Project
cd /tmp
# We only need the cves folder, so we use sparse-checkout to only get that folder
git clone -n --depth=1 --filter=tree:0 https://github.com/VulnerabilityHistoryProject/vulnerabilities.git
cd vulnerabilities
git sparse-checkout set --no-cone cves
git checkout
mv /tmp/vulnerabilities/cves/* /project/Data/DatasetSources/VulnerabilityHistoryProject/cves/

cd /project/Data/DatasetSources/
python VulnerabilityHistoryProject/import_vhp.py

# TypeCommonID, TypeCVEID, TypeCommitSha as stated in the README
python import_type_mappings.py

# Syzkaller
python syzkaller/crawl.py