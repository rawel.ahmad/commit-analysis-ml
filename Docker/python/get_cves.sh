#!/bin/bash
python3 /project/cve-search/sbin/db_mgmt_cpe_dictionary.py -p -v
python3 /project/cve-search/sbin/db_mgmt_json.py -p -v
# This will take > 45 minutes on a decent machine, please be patient
python3 /project/cve-search/sbin/db_updater.py -c -v