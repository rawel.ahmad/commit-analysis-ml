## Important Note

The code contained in this folder is not up-to-date or fully documented. This is because the code in this folder is not directly a part of the main thesis project. It may contain experimental, preliminary, or auxiliary scripts and files that were used for different purposes during the development process. The code in this folder is provided for reference only.

The code is adapted from the work:
Jan Philipp Wagner. “Evaluating and improving commit-based static analysis”. Bachelor’s
Thesis. Technische Universität Darmstadt. url: https://fileserver.
tk.informatik.tu-darmstadt.de/NA/Thesis_JW.pdf.