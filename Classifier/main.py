import os
from glob import glob
from multiprocessing import Pool

from joblib import load, dump
from scipy.sparse import load_npz
from tqdm import tqdm

from Classifier.BagOfWords import BagOfWords
from Classifier.commit_features import CommitFeatures
from Classifier.svm import Svm
from Data.Database.db_repository import DBRepository
from Data.Utils.utils import get_config_nodes_repo_dict

abspath = os.path.dirname(os.path.abspath(__file__))
os.chdir(abspath)


def store_commit(mapping: dict):
    print(f"PID {os.getpid()}: Storing VCC({mapping['vcc_sha']}) and Fixing({mapping['fixing_sha']})")
    if mapping["vcc_sha"] is not None:
        store_vcc(mapping)
    if mapping["fixing_sha"] is not None:
        store_fixing_commit(mapping)


def store_vcc(mapping: dict):
    if mapping["vcc_sha"] is None:
        return

    config_code = mapping["vcc_config_code"]
    repo_path = get_config_nodes_repo_dict()[config_code].find("./path").text
    directory = "Training/vccs"

    if mapping["determined_by_heuristic"] == 0:
        directory = f"{directory}/{config_code.lower()}/ground_truth"
    else:
        if mapping["confidence_value"] == 1:
            directory = f"{directory}/{config_code.lower()}/confident"
        else:
            directory = f"{directory}/{config_code.lower()}/not_confident"

    try:
        if os.path.exists(f"{directory}/{mapping['vcc_sha']}.json"):
            # Skip if already exists
            return
        commit_features = CommitFeatures(repo_path, mapping["vcc_sha"])
        commit_features.extract_features()
        commit_features.save_features_to_json(directory)
        print(f"PID {os.getpid()}: Stored VCC({mapping['vcc_sha']})")
    except ValueError as e:
        print(e)


def store_fixing_commit(mapping: dict):
    config_code = mapping["fixing_config_code"]
    repo_path = get_config_nodes_repo_dict()[config_code].find("./path").text
    directory = f"Training/unclassified/{config_code.lower()}"

    try:
        if os.path.exists(f"{directory}/{mapping['fixing_sha']}.json"):
            # Skip if already exists
            return
        commit_features = CommitFeatures(repo_path, mapping["fixing_sha"])
        commit_features.extract_features()
        commit_features.save_features_to_json(directory)
        print(f"PID {os.getpid()}: Stored Fixing({mapping['fixing_sha']})")
    except ValueError as e:
        print(e)


def evaluate(svm, vcc_vectors, unclassified_vectors, threshold=-99999999999999):
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    for vec in vcc_vectors:
        result = svm.vcc_or_unclassified(vec, threshold=threshold)
        if result:
            tp += 1
        else:
            fn += 1

    for vec in unclassified_vectors:
        result = svm.vcc_or_unclassified(vec, threshold=threshold)
        if result:
            fp += 1
        else:
            tn += 1

    print("True Positives: ", str(tp))
    print("True Negatives: ", str(tn))
    print("False Positives: ", str(fp))
    print("False Negatives: ", str(fn))

    precision = 0
    recall = 0
    accuracy = 0
    f = 0
    f2 = 0
    try:
        if tp + fp == 0:
            return [0, 0, 0, 0, 0]
        precision = tp / (tp + fp)
        recall = tp / (tp + fn)
        accuracy = (tp + tn) / (tp + tn + fp + fn)
        f = 2 * ((precision * recall) / (precision + recall))
        beta = 0.5
        f2 = (1 + beta ** 2) * ((precision * recall) / ((beta ** 0.5 * precision) + recall))
        jan = tp / fp
        print("F2: ", str(f2))
        print("Jan: ", str(jan))
        print("Precision: ", str(precision))
        print("Recall: ", str(recall))
    except:
        pass
        # print("Division by 0.")

    return [precision, recall, accuracy, f, f2]


def find_best_hyperparameters(
        svm, vcc_training, unclassified_training, vcc_validation, unclassified_validation, range_c, range_w
):
    pass


def store_commit_features():
    """Stores the commit features in a JSON file."""
    db_repo = DBRepository()
    mappings = db_repo.get_all_mappings()

    p = Pool()
    p.map(store_commit, mappings)
    p.close()
    p.join()

    print("Done!")


def create_feature_vectors():
    bag_of_words = BagOfWords()
    bag_of_words.load_bag_of_words()
    raw_data_paths = glob("**/*.json", recursive=True)

    for path in tqdm(raw_data_paths):
        # create feature vector
        commit = CommitFeatures()
        commit.load_features_from_json(path)
        commit.create_feature_vector(bag_of_words)
        # Remove the .json extension with the hash itself
        commit.store_feature_vector_in(path[:-45])


# def create_vcc_validation_set():
#     directory = "Validation/vccs"
#     vcc_dataset = load_vcc_dataset()
#
#     if not os.path.exists(directory):
#         os.makedirs(directory)
#
#     # Split the dataset into 80% training and 20% validation
#     split_index = int(len(vcc_dataset) * 0.8)
#     vcc_training = vcc_dataset[:split_index]
#     vcc_validation = vcc_dataset[split_index:]
#
#     # Save the validation set


def load_vcc_training_dataset(joblib_file=None):
    if joblib_file is not None:
        return load(joblib_file)

    vcc_training = []
    for feature_vector in glob("Training/vccs/*/ground_truth/*.npz"):
        vcc_training.append([load_npz(feature_vector), 5])
    for feature_vector in glob("Training/vccs/*/confident/*.npz"):
        vcc_training.append([load_npz(feature_vector), 5])
    for feature_vector in glob("Training/vccs/*/not_confident/*.npz"):
        vcc_training.append([load_npz(feature_vector), 1])
    print(f"Loaded {len(vcc_training)} VCCs")

    os.makedirs("Vectors", exist_ok=True)
    dump(vcc_training, "Vectors/vcc_training.joblib")
    return vcc_training


def load_unclassified_training_dataset(joblib_file=None):
    if joblib_file is not None:
        return load(joblib_file)

    unclassified_training = []
    for feature_vector in glob("Training/unclassified/*/*.npz"):
        unclassified_training.append([load_npz(feature_vector), feature_vector[-44:-4]])
    print(f"Loaded {len(unclassified_training)} unclassified commits")

    os.makedirs("Vectors", exist_ok=True)
    dump(unclassified_training, "Vectors/unclassified_training.joblib")

    return unclassified_training


def load_vcc_validation_dataset(joblib_file=None):
    if joblib_file is not None:
        return load(joblib_file)

    vcc_validation = []
    for feature_vector in glob("Validation/vccs/*/*.npz"):
        vcc_validation.append([load_npz(feature_vector), feature_vector[-44:-4]])
    print(f"Loaded {len(vcc_validation)} VCCs")

    os.makedirs("Vectors", exist_ok=True)
    dump(vcc_validation, "Vectors/vcc_validation.joblib")

    return vcc_validation


def load_unclassified_validation_dataset(joblib_file=None):
    if joblib_file is not None:
        return load(joblib_file)

    unclassified_validation = []
    for feature_vector in glob("Validation/unclassified/*/*.npz"):
        unclassified_validation.append([load_npz(feature_vector), feature_vector[-44:-4]])
    print(f"Loaded {len(unclassified_validation)} unclassified commits")

    os.makedirs("Vectors", exist_ok=True)
    dump(unclassified_validation, "Vectors/unclassified_validation.joblib")

    return unclassified_validation


def train_model(c=0.09, w=0.2):
    svm = Svm()

    vcc_training = load_vcc_training_dataset()
    unclassified_training = load_unclassified_training_dataset()

    svm.train_model(vcc_training, unclassified_training, c=c, weight=w)
    svm.save_model(f"c-{c}w{w}")


def main():
    # store_commit_features()
    # BagOfWords.create_bag_of_words()
    create_feature_vectors()
    train_model()


if __name__ == "__main__":
    main()
