import glob

from tqdm import tqdm

from Data.Database.db_repository import DBRepository


def get_config_codes_distribution():
    db_repo = DBRepository()
    commits = db_repo.get_all_commits()

    vcc_paths = glob.glob("Training/vccs/**/*.json", recursive=True)

    config_codes = {}

    """
    config_codes = {
        "chromium": {
            "2010": 0,
        }
    }
    """

    for path in tqdm(vcc_paths):
        if len(path.split("/")) == 5:
            [_, _, config_code, _, file_name] = path.split("/")
        else:
            [_, _, config_code, file_name] = path.split("/")
        com_sha = file_name.rstrip(".json")

        commit = list(filter(lambda com: com["com_sha"] == com_sha and com["com_config_code"] == config_code, commits))

        if len(commit) != 1:
            print(f"Error: {path}")
            continue

        if config_code not in config_codes:
            config_codes[config_code] = {}

        commit = commit[0]
        year = commit["com_date_committed"].year

        if year not in config_codes[config_code]:
            config_codes[config_code][year] = 0
        config_codes[config_code][year] += 1

    return config_codes
