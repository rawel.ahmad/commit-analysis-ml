import glob
import os

from tqdm import tqdm

from Data.Database.db_repository import DBRepository

abspath = os.path.dirname(os.path.abspath(__file__))
os.chdir(abspath)


def get_relevant_entry(com_sha, config_code, is_fixing):
    col = "fixing_sha" if is_fixing else "vcc_sha"

    entries = list(filter(lambda entry: entry[col] == com_sha and entry["fixing_config_code"] == config_code, mappings))

    if len(entries) == 1:
        return entries[0]

    ground_truth = list(filter(lambda entry: entry["determined_by_heuristic"] == 0, entries))
    if len(ground_truth) >= 1:
        return ground_truth[0]

    confident = list(filter(lambda entry: entry["confidence_value"] == 1, entries))
    if len(confident) >= 1:
        return confident[0]

    not_confident = list(filter(lambda entry: entry["confidence_value"] == 0, entries))
    if len(not_confident) >= 1:
        return not_confident[0]


def get_path(mapping, is_fixing):
    directory = "Training/unclassified" if is_fixing else "Training/vccs"
    config_code = mapping["fixing_config_code"]

    if mapping["determined_by_heuristic"] == 0:
        directory = f"{directory}/{config_code.lower()}/ground_truth"
    else:
        if mapping["confidence_value"] == 1:
            directory = f"{directory}/{config_code.lower()}/confident"
        else:
            directory = f"{directory}/{config_code.lower()}/not_confident"

    os.makedirs(directory, exist_ok=True)
    return directory


def main():
    global mappings
    db_repo = DBRepository()
    mappings = db_repo.get_all_mappings()

    vcc_paths = glob.glob("Training/vccs/**/*.json", recursive=True)

    for path in tqdm(vcc_paths):
        [_, _, config_code, _, file_name] = path.split("/")
        com_sha = file_name.rstrip(".json")

        mapping = get_relevant_entry(com_sha, config_code, False)
        end_directory = f"{get_path(mapping, False)}/{file_name}"
        os.rename(path, end_directory)


if __name__ == "__main__":
    main()
