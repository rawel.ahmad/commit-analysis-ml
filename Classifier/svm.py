import os
import sys

import numpy as np
from joblib import dump, load
from scipy.sparse import hstack, vstack
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import KBinsDiscretizer
from sklearn.svm import LinearSVC

sys.setrecursionlimit(100000000)
abspath = os.path.dirname(os.path.abspath(__file__))
os.chdir(abspath)


class Svm:
    def __init__(self):
        self.confidences = {}
        self.model = None
        self.preprocessing = None

    def load_model(self, model_name):
        self.model = load(f"Models/{model_name}.joblib")
        self.preprocessing = load(f"Models/{model_name}_preprocessing.joblib")

    def save_model(self, model_name):
        os.makedirs("Models", exist_ok=True)
        dump(self.model, f"Models/{model_name}.joblib")
        dump(self.preprocessing, f"Models/{model_name}_preprocessing.joblib")
        print(f"Saved model to {model_name}.joblib! Parameters:\n\n{str(self.model)}")

    def vcc_or_unclassified(self, feature_vector, bag_of_words, threshold=1):
        if feature_vector[1] not in self.confidences.keys():
            scaled_feature_vector = self.preprocess(feature_vector[0])
            confidence = self.model.decision_function(scaled_feature_vector)
            # significance_vector = scaled_feature_vector.multiply(self.model.coef_[0])
            self.confidences[feature_vector[1]] = confidence
        else:
            confidence = self.confidences[feature_vector[1]]

        if confidence[0] > threshold:
            # print("Commit", str(feature_vector[1]), "is prone to be vulnerable!")
            """
            print("Confidence:", str(confidence[0]))
            print("The significant feature was:" , str(significance_vector.argmax()), "with score", str(significance_vector.max()) + "\n")
            """
            return True
        else:
            # print("Commit", str(feature_vector[1]), "is not prone to be vulnerable!")
            """
            print("Confidence:", str(confidence[0]))
            print("The significant feature was: " + str(significance_vector.argmin()), "with score", str(significance_vector.min()) + "\n")
            """
            return False

    def evaluate_set(self, vcc_feature_vectors, unclassified_feature_vectors):
        """"""
        """
        feature_vectors = self.preprocess(vcc_feature_vectors[0][0])
        for feature_vector in vcc_feature_vectors[1:]+unclassified_feature_vectors:
            feature_vectors = vstack((feature_vectors, self.preprocess(feature_vector[0])))
        dump(feature_vectors, "Vectors/testing_set.joblib")
        """
        feature_vectors = load("Vectors/testing_set.joblib")

        confidences = self.model.decision_function(feature_vectors)
        for i, confidence in enumerate(confidences):
            self.confidences[(vcc_feature_vectors + unclassified_feature_vectors)[i][1]] = confidence

        pre = []
        rec = []
        for t in np.arange(-5, 5, 0.001):
            tp = 0
            tn = 0
            fn = 0
            fp = 0
            for commit in vcc_feature_vectors:
                if self.confidences[commit[1]] > t:
                    tp += 1
                else:
                    fn += 1

            for commit in unclassified_feature_vectors:
                if self.confidences[commit[1]] > t:
                    fp += 1
                else:
                    tn += 1

            if tp == 0:
                precision = 0
                recall = 0
                pre.append(0)
                rec.append(0)
            else:
                precision = tp / (tp + fp)
                recall = tp / (tp + fn)
                pre.append(precision)
                rec.append(recall)
            print("True Positives: ", str(tp))
            print("True Negatives: ", str(tn))
            print("False Positives: ", str(fp))
            print("False Negatives: ", str(fn))
            print("Precision: ", str(precision))
            print("Recall: ", str(recall))

        open("x_own", "w+").write(str(rec))
        open("y_own", "w+").write(str(pre))

    def train_model(self, vcc_feature_vectors_and_weights, unclassified_feature_vectors, fs=303, c=1, weight=1):
        labels = []
        weights = []
        vcc_feature_vectors = [x[0] for x in vcc_feature_vectors_and_weights]
        self.prepare_preprocessing(vcc_feature_vectors_and_weights, unclassified_feature_vectors)
        feature_vectors = self.get_feature_vectors(vcc_feature_vectors, unclassified_feature_vectors)

        for i, vector in enumerate(vcc_feature_vectors_and_weights):
            labels.append(1)
            weights.append(vcc_feature_vectors_and_weights[i][1] * weight)
        for i in range(len(unclassified_feature_vectors)):
            labels.append(0)
            weights.append(1)

        print("fitting...")
        self.model = LinearSVC(C=c, max_iter=100000000)
        self.model.fit(feature_vectors, labels, weights)
        print("Score:", str(self.model.score(feature_vectors, labels)))
        print("Done")
        self.confidences = {}
        k = 5
        scores = cross_val_score(self.model, feature_vectors, labels, cv=k)
        print(scores)
        print("Average score:", str(sum(scores) / k))

    def get_feature_vectors(self, vcc_feature_vectors, unclassified_feature_vectors, feature_vector_path=None):
        if feature_vector_path is not None:
            return load(feature_vector_path)

        feature_vectors = self.preprocess(vcc_feature_vectors[0])
        for feature_vector in vcc_feature_vectors[1:] + unclassified_feature_vectors:
            feature_vectors = vstack((feature_vectors, self.preprocess(feature_vector)))

        os.makedirs("Vectors", exist_ok=True)
        dump(feature_vectors, "Vectors/feature_vectors.joblib")
        return feature_vectors

    def prepare_preprocessing(self, vcc_feature_vectors, unclassified_feature_vectors, preprocessing_path=None):
        if preprocessing_path is not None:
            self.preprocessing = load(preprocessing_path)
            return

        vcc_feature_vectors = [x[0] for x in vcc_feature_vectors]
        fit_vectors = [x.tocsc()[0, :303] for x in vcc_feature_vectors + unclassified_feature_vectors]
        # preprocess fit
        self.preprocess_fit(fit_vectors)
        os.makedirs("Models", exist_ok=True)
        dump(self.preprocessing, "Models/preprocessing.joblib")

    def preprocess_fit(self, vectors):
        fit_vecs = vectors[0]
        for vec in vectors:
            fit_vecs = vstack((fit_vecs, vec))
        self.preprocessing = KBinsDiscretizer(n_bins=5)
        self.preprocessing.fit(fit_vecs.toarray())

    def preprocess(self, vector):
        return hstack(
            (self.preprocessing.transform(vector.tocsc()[0, :303].toarray()), vector.tocsc()[0, 303:].toarray())
        ).tocsr()
