import glob
import os
import random
from multiprocessing import Pool

from Classifier.code_utils import get_config_codes_distribution
from Classifier.commit_features import CommitFeatures
from Data.Database.db_repository import DBRepository
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import get_config_nodes_repo_dict, get_config_codes_list

abspath = os.path.dirname(os.path.abspath(__file__))
os.chdir(abspath)


def choose_random_wrapper(config_code, year):
    print(f"PID {os.getpid()}: Choosing random for {config_code} in {year}")
    db_repo = DBRepository()
    save_dir = f"Extra/unclassified/{config_code.lower()}"
    max_commits = distribution[config_code][year] * 3
    choose_random(db_repo, config_code, year, save_dir, max_commits)


def choose_random(db_repo, config_code, year, save_dir, max_commits):
    repo_node = get_config_nodes_repo_dict()[config_code]
    commit_utils = CommitUtils(False, repo_node, None)
    vccs = set(db_repo.get_all_vccs())
    commits = commit_utils.get_commits_between_years(year, year)
    commits = list(filter(lambda com: com.hexsha not in vccs, commits))

    already_saved = set(glob.glob(f"{save_dir}/*.json"))
    already_saved = {os.path.basename(f).rstrip(".json") for f in already_saved}
    commit_shas = {commit.hexsha for commit in commits}

    i = len(already_saved.intersection(commit_shas))

    for _ in commits:
        try:
            if i >= max_commits:
                print(f"PID {os.getpid()}: Saved {i} commits for {config_code} in {year}")
                break
            random_commit = random.choice(commits)
            if random_commit.hexsha in already_saved:
                continue
            commit_features = CommitFeatures(repo_node.find("./path").text, random_commit.hexsha)
            commit_features.extract_features()
            commit_features.save_features_to_json(save_dir)
            db_repo.save_commit(random_commit, config_code)
            i += 1
            already_saved.add(random_commit.hexsha)
            print(f"PID {os.getpid()}: Saved {random_commit.hexsha} for {config_code} in {year}")
        except ValueError as e:
            print(e)
        except Exception as e:
            print(f"PID {os.getpid()}: Error while saving commit for {config_code} in {year}")


def store_unclassified_training():
    years = [year for year in range(1998, 2019)]
    config_codes = [code for code in get_config_codes_list()]
    p = Pool()
    p.starmap(choose_random_wrapper, [(code, year) for code in config_codes for year in years])
    p.close()
    p.join()
    print("Done")


def main():
    global distribution
    distribution = get_config_codes_distribution()
    store_unclassified_training()


if __name__ == "__main__":
    main()
