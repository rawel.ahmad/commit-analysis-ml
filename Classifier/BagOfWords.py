import glob
import json
import os
import subprocess

from joblib import dump
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from tqdm import tqdm


class BagOfWords:
    def __init__(self):
        self.word_vect = CountVectorizer()
        self.word_transformer = TfidfTransformer()

    def load_bag_of_words(self):
        print("Loading bag of words")
        with open("words.txt", "r+") as w:
            word_counts = self.word_vect.fit_transform(w)
            self.word_transformer.fit_transform(word_counts)

    @staticmethod
    def create_bag_of_words():
        all_words = open("words.c", "w+")

        for path in tqdm(
            glob.glob("Training/vccs/**/*.json", recursive=True)
            + glob.glob("Training/unclassified/**/*.json", recursive=True),
            desc="Creating bag of words",
        ):
            with open(path, "r+") as json_file:
                raw_data = json.load(json_file)
                words = raw_data["added_code"] + "\n"
                words += raw_data["deleted_code"] + "\n"
                words += raw_data["commit_message"] + "\n"

                all_words.write(words.encode("utf8", "replace").decode("utf8"))
        all_words.close()

        # Remove comments
        subprocess.call(["./remove_comments.sh"])
        os.remove("words.c")

    def get_word_vect(self, words):
        new_word_counts = self.word_vect.transform(words)
        return self.word_transformer.transform(new_word_counts)

    def save_bag_of_words(self, name):
        dump(self, f"Models/{name}_bag_of_words.joblib")
