import pandas as pd
from tqdm import tqdm

from Data.Database.db_repository import DBRepository
from Data.DatasetSources.linux_kernel_cves import KERNEL_CVES_FILE
from Data.Utils.CVESearch import CVESearch
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import get_config_nodes_repo_dict

config_code = "kernel"


def main():
    """
    This script imports the Linux Kernel CVEs from the JSON file to the DB
    :return:
    """
    repo_node = get_config_nodes_repo_dict()[config_code]
    df = pd.read_json(KERNEL_CVES_FILE, orient="index")

    df_temp = df[["breaks", "fixes"]].copy()
    df_temp = df_temp.replace("", None, regex=True)
    db_repo = DBRepository()

    # Each CVE and commit should be saved in the DB
    cve_search = CVESearch(True, config_code)
    commit_utils = CommitUtils(True, repo_node, config_code)

    for index, row in tqdm(df_temp.iterrows(), desc="Saving VCCs and Fixing Commits", total=df_temp.shape[0]):
        try:
            vcc = None
            fixing_commit = None
            # Save fixing and vccs
            if pd.notna(row["fixes"]):
                fixing_commit = commit_utils.get_commit_by_sha(row["fixes"])
            if pd.notna(row["breaks"]):
                vcc = commit_utils.get_commit_by_sha(row["breaks"])
            cve_search.get_cve_by_id(index)

            db_repo.save_vcc_fixing_commit(
                index,
                fixing_commit.hexsha if fixing_commit else None,
                vcc.hexsha if vcc else None,
                config_code,
                "LinuxKernelCVEs",
                None
            )
        except Exception as e:
            print(f"Could not save {index} - {e} ...skipping")
            continue


if __name__ == "__main__":
    main()
