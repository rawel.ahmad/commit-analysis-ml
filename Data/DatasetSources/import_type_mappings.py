import re

from tqdm import tqdm

from Data.Database.db_repository import DBRepository
from Data.Utils.CVESearch import CVESearch
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import get_config_node, get_config_codes_list, extract_id


def import_type_common_id(config_code: str):
    """
    Import commits from a repository that have a common id in their commit message
    :param config_code: The config code of the product
    """
    product_node = get_config_node(config_code)
    try:
        commit_utils = CommitUtils(False, product_node.find("./mapping/repo"), config_code)
    except OSError:
        print(f"No repository found for {config_code}")
        return
    cve_search = CVESearch(True, config_code)
    common_id_mapping = product_node.findall(f"./mapping[@type='TypeCommonID']")
    db_repo = DBRepository()

    if not common_id_mapping:
        return

    for mapping_node in common_id_mapping:
        cve_list = cve_search.get_map_from_common_id_to_cve(mapping_node)
        cve_mappings = commit_utils.map_common_ids_to_commits(cve_list)

        for cve_id, commit_list in tqdm(cve_mappings.items(), desc="Saving commits"):
            # Save the cve, commit and its relation in the database
            cve_search.get_cve_by_id(cve_id)
            for commit in commit_list:
                db_repo.save_commit(commit, config_code)
                db_repo.save_vcc_fixing_commit(cve_id, commit.hexsha, None, config_code, "TypeCommonID", None)


def import_type_cve_id(config_code: str):
    """
    Import commits from a repository that have a cve id in their commit message
    :param config_code: The config code of the product
    """
    product_node = get_config_node(config_code)
    try:
        commit_utils = CommitUtils(False, product_node.find("./mapping/repo"), config_code)
    except OSError:
        print(f"No repository found for {config_code}")
        return
    cve_search = CVESearch(True, config_code)
    cves = cve_search.get_cves_by_cpe(product_node.find("./mapping/nvd/cpe").text)
    cve_ids = [cve["id"] for cve in cves]
    cve_id_mapping = product_node.findall(f"./mapping[@type='TypeCVEID']")
    db_repo = DBRepository()

    if not cve_id_mapping:
        return

    for mapping_node in cve_id_mapping:
        commits = commit_utils.get_list_of_commits()
        for commit in tqdm(commits, desc="Finding cves in commit messages"):
            for regex_node in mapping_node.findall("./repo/regex-list/regex"):
                regex = regex_node.find("./contains").text
                search = re.search(regex, commit.message)
                if search:
                    cve_id = search.group()
                    if cve_id not in cve_ids:
                        continue
                    cve_search.get_cve_by_id(cve_id)
                    db_repo.save_commit(commit, config_code)
                    db_repo.save_vcc_fixing_commit(cve_id, commit.hexsha, None, config_code, "TypeCVEID", None)


def import_type_commit_sha(config_code: str):
    """
    Import commits whose sha is in the cve references
    :param config_code: The config code of the product
    """
    product_node = get_config_node(config_code)
    cve_search = CVESearch(True, config_code)
    try:
        commit_utils = CommitUtils(False, product_node.find("./mapping/repo"), config_code)
    except OSError:
        print(f"No repository found for {config_code}")
        return
    cves = cve_search.get_cves_by_cpe(product_node.find("./mapping/nvd/cpe").text)
    cve_id_mapping = product_node.findall(f"./mapping[@type='TypeCommitSha']")
    db_repo = DBRepository()

    if not cve_id_mapping:
        return

    for mapping_node in cve_id_mapping:
        for cve in tqdm(cves, desc="Finding commits from cve references"):
            cve_id = cve["id"]
            for regex_node in mapping_node.findall("./nvd/regex-list/regex"):
                regex = regex_node.find("./contains").text
                commit_candidates = [ref for ref in cve["references"] if re.search(regex, ref)]

                if not commit_candidates:
                    continue

                for commit_candidate in commit_candidates:
                    commit_shas = extract_id(regex_node, commit_candidate)
                    for commit_sha in commit_shas:
                        try:
                            commit = commit_utils.get_commit_by_sha(commit_sha)
                            if commit:
                                cve_search.get_cve_by_id(cve_id)
                                db_repo.save_commit(commit, config_code)
                                db_repo.save_vcc_fixing_commit(cve_id, commit.hexsha, None, config_code, "TypeCommitSha", None)
                        except Exception:
                            print(f"Commit {commit_sha} not found in repository {config_code}")
                            continue


def main():
    config_codes_list = get_config_codes_list()

    for config_code in config_codes_list:
        print(f"Importing TypeCommonID for {config_code}")
        import_type_common_id(config_code)

    for config_code in config_codes_list:
        print(f"Importing TypeCVEID for {config_code}")
        import_type_cve_id(config_code)

    for config_code in config_codes_list:
        print(f"Importing TypeCommitSha for {config_code}")
        import_type_commit_sha(config_code)


if __name__ == "__main__":
    main()
