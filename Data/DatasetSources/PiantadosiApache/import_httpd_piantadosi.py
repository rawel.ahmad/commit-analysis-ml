import pandas as pd
from tqdm import tqdm

from Data.Database.db_repository import DBRepository
from Data.DatasetSources.PiantadosiApache import PIANTADOSI_FILE
from Data.Utils.CVESearch import CVESearch
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import get_config_nodes_repo_dict

config_code = "httpd"


def main():
    """
    This script imports the fixing commits from the Piantadosi dataset for the Apache project.
    """
    df = pd.read_csv(PIANTADOSI_FILE)
    config_nodes = get_config_nodes_repo_dict()
    cve_search = CVESearch(db=True, config_code=config_code)
    db_repo = DBRepository()
    xml_path = config_nodes.get(config_code)
    if xml_path is None:
        raise ValueError("Make sure to define the httpd node in the config.xml file")

    df_mappings = df[["cve_id", "fix_commit_id"]]

    try:
        commit_utils = CommitUtils(True, xml_path, config_code)
    except Exception:
        # If the commit utils cannot be initialized, then we can only save the commit sha
        commit_utils = None

    for idx, row in tqdm(df_mappings.iterrows(), desc="Processing fixing commits", total=df_mappings.shape[0]):
        try:
            cve_search.get_cve_by_id(row["cve_id"])
            commit = None
            if commit_utils is not None:
                commit_utils.get_commit_by_sha(row["fix_commit_id"])
            else:
                db_repo.save_commit(row["fix_commit_id"], config_code=config_code)
        except Exception as e:
            tqdm.write(f"Error: {e}")
            db_repo.save_commit(row["fix_commit_id"], config_code=config_code)
        finally:
            db_repo.save_vcc_fixing_commit(row["cve_id"], row["fix_commit_id"], None, config_code, "PiantadosiApache", None)


if __name__ == "__main__":
    main()
