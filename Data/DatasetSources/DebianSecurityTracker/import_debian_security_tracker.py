import re
from itertools import product

from tqdm import tqdm

from Data.Database.db_repository import DBRepository
from Data.DatasetSources.DebianSecurityTracker import FILE_PATH
from Data.Utils.CVESearch import CVESearch
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import (
    contains_string,
    contains_commit_sha,
    extract_commit_sha,
    is_vulnerable_commit,
    contains_url,
    extract_repository_and_commit_id,
    extract_url,
    get_config_nodes_repo_dict,
)

# Structure of each entry in the dict: {"cve_id":{"fix_commit_sha": list, "vcc_sha": list, "config_code": str}}
# The fix_commit_sha list contains all the commit shas of the fixing commits
# The vcc_sha list contains all the commit shas of the vccs
cves = {}

# Structure of each entry in the dict: {"config_code": "repo_path"}
config_codes = get_config_nodes_repo_dict()


def map_to_path(config_code: str):
    """
    Maps the given config code to the corresponding repo path
    :param config_code: the config code
    :return: the repo path or None if no mapping was found
    """
    return config_codes.get(config_code)


def add_to_cves(cve_id, fix_commit_sha, vcc_sha, config_code):
    """
    Adds the given information to the cves dict
    :param cve_id: the cve id
    :param fix_commit_sha: commit sha of the fixing commit
    :param vcc_sha: commit sha of the vcc
    :param config_code: the config code
    """
    if cve_id in cves:
        if fix_commit_sha:
            cves[cve_id]["fix_commit_sha"].append(fix_commit_sha)
        if vcc_sha:
            cves[cve_id]["vcc_sha"].append(vcc_sha)
    else:
        cves[cve_id] = {
            "fix_commit_sha": [fix_commit_sha] if fix_commit_sha else [],
            "vcc_sha": [vcc_sha] if vcc_sha else [],
            "config_code": config_code,
        }


def import_cves():
    """
    Imports the cves from the cve-search database into the cves dict
    """
    current_cve_id = None

    with open(FILE_PATH, "r+") as f:
        for line in f.readlines():
            if line.startswith("CVE-"):
                # CVE
                search = re.search(r"CVE-\d{4}-\d{4,7}", line)
                if search:
                    current_cve_id = search.group(0)
            found = False
            for config_code in config_codes.keys():
                if contains_string(config_code, line) and contains_url(line) and contains_commit_sha(line):
                    commit_sha = extract_commit_sha(line)
                    if is_vulnerable_commit(line):
                        add_to_cves(current_cve_id, None, commit_sha, config_code)
                    else:
                        # Assume that the commit is a fix commit if it is not explicitly marked as a vulnerable commit
                        add_to_cves(current_cve_id, commit_sha, None, config_code)
                    found = True
                    break
            if found is False and contains_string("github.com", line) and contains_commit_sha(line):
                # Only try to extract the repo if it is not already mapped to a config code
                # Any commit from GitHub which is not a pull request
                try:
                    repository, commit_sha = extract_repository_and_commit_id(extract_url(line))
                except ValueError:
                    # The url is not a GitHub url
                    continue
                if is_vulnerable_commit(line):
                    add_to_cves(current_cve_id, None, commit_sha, repository)
                else:
                    add_to_cves(current_cve_id, commit_sha, None, repository)


def normalize_cves():
    """
    Normalizes the cves dict into a list of dicts to add to the database.
    Normalization means that each entry in the cves dict is split into multiple entries in the normalized_cves list.
    This is necessary because a CVE can have multiple fix commits and multiple vcc commits.
    :return: The normalized cves
    """
    normalized_cves_ = []

    for cve_id, cve in tqdm(cves.items(), desc="Normalizing CVEs"):
        fix_commit_sha_list = cve["fix_commit_sha"]
        vcc_sha_list = cve["vcc_sha"]

        if len(fix_commit_sha_list) > 0 and len(vcc_sha_list) > 0:
            # Create a combination of all fix commits, and vcc commits
            # This is necessary because a CVE can have multiple fix commits and multiple vcc commits
            commit_combinations = list(product(fix_commit_sha_list, vcc_sha_list))
            for fix_commit_sha, vcc_sha in commit_combinations:
                normalized_cves_.append(
                    {
                        "cve_id": cve_id,
                        "fixing_sha": fix_commit_sha,
                        "vcc_sha": vcc_sha,
                        "fixing_config_code": cve["config_code"],
                        "vcc_config_code": cve["config_code"],
                        "mapping_type": "DebianSecurityTracker",
                    }
                )
        elif len(fix_commit_sha_list) > 0:
            # If only the fix commits are available, only create a mapping for the fix commits
            for fix_commit_sha in fix_commit_sha_list:
                normalized_cves_.append(
                    {
                        "cve_id": cve_id,
                        "fixing_sha": fix_commit_sha,
                        "vcc_sha": None,
                        "fixing_config_code": cve["config_code"],
                        "vcc_config_code": cve["config_code"],
                        "mapping_type": "DebianSecurityTracker",
                    }
                )
        elif len(vcc_sha_list) > 0:
            # If only the vcc commits are available, only create a mapping for the vcc commits
            for vcc_sha in vcc_sha_list:
                normalized_cves_.append(
                    {
                        "cve_id": cve_id,
                        "fixing_sha": None,
                        "vcc_sha": vcc_sha,
                        "fixing_config_code": cve["config_code"],
                        "vcc_config_code": cve["config_code"],
                        "mapping_type": "DebianSecurityTracker",
                    }
                )

    return normalized_cves_


def insert_into_database(normalized_cves):
    """
    Inserts the normalized cves into the database
    :param normalized_cves: the normalized cves
    """
    db_repo = DBRepository()
    cve_search = CVESearch(False, None)
    for cve in tqdm(normalized_cves, desc="Inserting CVEs into database"):
        # 1. Save the CVE
        cve_dict = cve_search.get_cve_by_id(cve["cve_id"])
        if cve_dict is None:
            db_repo.save_cve(cve["cve_id"], cve["fixing_config_code"])
        else:
            db_repo.save_cve(cve_dict, cve["fixing_config_code"])

        # 2. Save the commits
        path = map_to_path(cve["fixing_config_code"])
        if path is None:
            # If the path is None, it means that only the commit sha is available
            if cve["fixing_sha"] is not None:
                db_repo.save_commit(cve["fixing_sha"], cve["fixing_config_code"])
            if cve["vcc_sha"] is not None:
                db_repo.save_commit(cve["vcc_sha"], cve["vcc_config_code"])
        else:
            try:
                commit_utils = CommitUtils(False, path, None)
                # Since there are many commits, we do not want to fetch from remote for each commit
                fixing_commit = commit_utils.get_commit_by_sha(cve["fixing_sha"], False)
                vcc_commit = commit_utils.get_commit_by_sha(cve["vcc_sha"], False)

                db_repo.save_commit(fixing_commit, cve["fixing_config_code"])
                db_repo.save_commit(vcc_commit, cve["vcc_config_code"])
            except OSError:
                tqdm.write(f'Could not find repository for "{cve["fixing_config_code"]}"')
                db_repo.save_commit(cve["fixing_sha"], cve["fixing_config_code"])
                db_repo.save_commit(cve["vcc_sha"], cve["vcc_config_code"])
            except Exception as e:
                tqdm.write(f'Exception: "{e}" for "{cve["fixing_config_code"]}"')
                # In case of an error, save only the commit sha, this might include
                # commits which are not in the repository, invalid commit sha, etc.
                db_repo.save_commit(cve["fixing_sha"], cve["fixing_config_code"])
                db_repo.save_commit(cve["vcc_sha"], cve["vcc_config_code"])

        db_repo.save_vcc_fixing_commit(
            cve["cve_id"], cve["fixing_sha"], cve["vcc_sha"], cve["fixing_config_code"], cve["mapping_type"], None
        )


def main():
    import_cves()
    _normalized_cves = normalize_cves()
    insert_into_database(_normalized_cves)


if __name__ == "__main__":
    main()
