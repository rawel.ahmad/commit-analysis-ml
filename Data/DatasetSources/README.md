# Dataset

This directory contains the dataset sources for VCCs.
This repository comes with already downloaded data.
The repo contains its own data for the GroundTruth. (As of 04/24/2023).

## Ground Truth Data

### DebianSecurityTracker

The data is extracted from the [Debian Security Tracker](https://security-tracker.debian.org/tracker/).
Specifically from
the [Repository](https://salsa.debian.org/security-tracker-team/security-tracker/-/blob/master/data/CVE/list).

Every CVE is saved in the database with its references, products, etc. If a CVE is not present in the database,
only the CVE ID is saved.
If a CVE contains notes with commit hashes, the commit hashes are extracted and saved in the database.
If a repo path is present and valid in the `config.xml` file, the commit hashes are checked against the repository and
additional information such as a commit message, authorship etc. is extracted.

Make sure that the repositories are cloned and their path is set in the `config.xml` file under the `repo` tag.

Download the list of CVEs provided above and put it into the `DebianSecurityTracker` folder.

### LinuxKernelCVEs

The data is extracted from the [GitHub Repository](https://github.com/nluedtke/linux_kernel_cves/tree/master/data)
provided by [nluedtke](https://github.com/nluedtke).
The data contains the CVEs, the breaking commit and the VCC from the upstream Linux Kernel.

Download the `kernel_cves.json` file and put it into the `linux_kernel_cves` folder.

### VulnerabilityHistoryProject

The data is extracted from the [GitHub Repository](https://github.com/VulnerabilityHistoryProject/vulnerabilities).
It contains CVEs, breaking commit and VCC. Under the folder `cves` there are folders for each product
with yml-files for each CVE.

The curation level of each CVE defines whether a CVE was curated manually or automatically.
If the curation level is 1, it was curated manually and can be considered as Ground Truth Data. This information
is also saved in the Database as manual and automatic respectively.

Download the repository and put the folders into the `VulnerabilityHistoryProject/cves` folder.

### PiantadosiApache

The data is obtained from Piandatosi et al.
(Fixing of security vulnerabilities in open source projects: A case study of apache HTTP server and apache tomcat).
It contains the CVEs and their fixing commits from the HTTPD project.

### Syzkaller

Syzkaller is an automated kernel fuzzing tool designed for Linux.
It continuously fuzzes main Linux kernel branches, seeking out bugs and vulnerabilities.
The crashes and their status are available on the [Syzbot Dashboard](https://syzkaller.appspot.com/upstream/fixed).
The website is crawled to extract fixing commits and VCCs if any.

In some cases, these crashes are attached to a CVE,
which are also extracted by finding fixing commits in the database which already have a CVE attached to them.
For the sake of completeness, we also extract the syzkaller ID,
its url and other information which can be found in the database schema.

### OSS-Fuzz

OSS-Fuzz is a continuous fuzzing service for open source software.
It is similar to Syzkaller, but fuzzes more than just the Linux kernel, e.g., browsers, compilers, etc.
The bugs can be found on
the [OSS-Fuzz Dashboard](https://bugs.chromium.org/p/oss-fuzz/issues/list?q=status%3AFixed%2CVerified%20Type%3DBug%2CBug-Security&can=1).

The focus is on both bugs and vulnerabilities that are marked as `Bug` and `Bug-Security` respectively.
The data is obtained by calling the API of the dashboard to extract data such as the title but also a 
commit range which contains the fixing commit and in some cases also a regressed commit range.

#### X-XSRF-Token

To run the crawler, you need to set the X-XSRF-Token in the `oss-fuzz/credentials.py`.
To find the token, follow these steps:

1. Open the [OSS-Fuzz Dashboard](https://bugs.chromium.org/p/oss-fuzz/issues/list?q=status%3AFixed%2CVerified%20Type%3DBug%2CBug-Security&can=1) in your browser.
2. Open the developer tools. Usually, you can do this by pressing `F12`.
3. Open the network tab and filter by `XHR`.
4. You should see multiple issues requests. Click on one of them.
5. In the header section of this request headers, you should see a header called `X-XSRF-Token`.
6. Copy the value of this header and paste it into the `credentials.py` file.

In case you do not see any requests, you need to reload the page.

![1.png](1.png)
*Figure 1: Finding the X-XSRF-Token in the developer tools. In this case for Google Chrome*

> **NOTE:** The token is only valid for a short period of time.
> If the crawler does not work, you need to update the token.
> However, no progress is lost since the crawler only checks for new issues.
> Otherwise, you will get a `403 Forbidden` error.