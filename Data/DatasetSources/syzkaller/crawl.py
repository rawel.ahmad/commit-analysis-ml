# Adapted from:
# [BIC-Tracker](https://github.com/JayJayJay1/BIC-Tracker/)
# See: https://github.com/JayJayJay1/BIC-Tracker/blob/main/autobisect/crawler/crawl.py

import datetime
import json
import logging
import os
import re
import sys
from itertools import product

import pandas as pd
import requests
from bs4 import BeautifulSoup
from git import Commit
from tqdm import tqdm

from Data.Database.db_repository import DBRepository
from Data.DatasetSources.syzkaller import DIST_DIR
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import get_config_nodes_repo_dict

linux_url = "https://syzkaller.appspot.com/upstream/fixed"


def main():
    """
    Fetches crashes from syzbot and parses them into a json file
    """
    logging.info("Fetching crashes from syzbot...")
    page = rate_limited_get(linux_url)

    logging.info("Parsing html...")
    crashes = parse_crashes_table(page.content)

    logging.info(f"Total crashes: {len(crashes.index)}")

    crash_bisect_success = crashes[crashes["Cause bisect"] == "done"]
    failed = crashes[crashes["Cause bisect"].isin(["error", "inconclusive", "unreliable"])]
    untested = crashes[crashes["Cause bisect"].str.strip() == ""]

    logging.info(f"Crashes which contain successful bisect: {len(crash_bisect_success.index)}")
    logging.info(f"Crashes whose bisect is unreliable: {len(failed.index)}")
    logging.info(f"Crashes whose bisect is not done yet: {len(untested.index)}")

    logging.info("Fetching bisection logs...")

    # Use dict now to make it easier to add nested fields
    crashes = crashes.to_dict("records")

    crashes = fetch_crashes(crashes)

    crashes_with_bics = fetch_bics(crashes)
    crashes_bic_available = [crash for crash in crashes_with_bics if len(crash["bics"]) > 0]
    crashes_bic_not_available = [crash for crash in crashes_with_bics if len(crash["bics"]) == 0]

    logging.info(f"Total crashes with bics: {len(crashes_with_bics)}")
    logging.info(f"Total crashes with bics available: {len(crashes_bic_available)}")
    logging.info(f"Total crashes with bics not available: {len(crashes_bic_not_available)}")

    logging.info("Writing crashes to directory...")
    write_crashes_to_dir(crashes_with_bics)


def rate_limited_get(url):
    return requests.get(url)


def parse_crashes_table(content):
    """
    Parses the crashes table from the html content
    :param content: html content
    :return: pandas dataframe containing the crashes table
    """
    soup = BeautifulSoup(content, "lxml")
    results = soup.find(class_="list_table")
    df = pd.read_html(str(results), header=0, extract_links="body")[0]
    df["Link"] = df.apply(lambda row: "https://syzkaller.appspot.com" + row["Title"][1], axis=1)
    # Clean up to remove tuples in the dataframe which do not have a link
    df = df.applymap(lambda x: x[0] if isinstance(x, tuple) else x)
    df["Id"] = df.apply(lambda row: row["Link"].split("=")[-1], axis=1)

    return df


def fetch_crashes(crashes):
    """
    Fetches the crashes from syzbot.
    This includes the fix commits and the bisection commits.
    It also filters out crashes which are already in the database.
    :param crashes: Crashes to fetch
    :return: Crashes with the fix commits and bisection commits.
    """
    db_repo = DBRepository()
    syzkaller_ids = db_repo.get_all_syzkaller_ids()

    # Filter crashes to only include crashes which are not already saved in the database
    filtered_crashes = list(filter(lambda c: c not in syzkaller_ids, crashes))

    for crash in tqdm(filtered_crashes, desc="Fetching crashes from syzbot"):
        crash_html = BeautifulSoup(rate_limited_get(crash["Link"]).content, "lxml")

        # Get the fix commits
        crash["fix-commits"] = extract_fixing_commits(crash["Id"], crash_html)

        # Get the bisection commits
        crash["fix-bisection-commit"] = extract_bisection_commit(crash["Id"], crash_html, True)
        crash["cause-bisection-commit"] = extract_bisection_commit(crash["Id"], crash_html, False)

    return filtered_crashes


def extract_bisection_commit(syzkaller_id, crash_html, is_fixing_commit: bool):
    """
    Extract the bisection commit from the crash html
    :param syzkaller_id: Syzkaller id of the crash.
    :param crash_html: HTML page of the crash.
    :param is_fixing_commit: True if the fixing commit is being extracted, False if the bisection commit is being
    extracted
    :return: The bisection commit if it exists, None otherwise
    """
    search_str = "Fix bisection: fixed by" if is_fixing_commit else "Cause bisection: introduced by"

    if not crash_html.find("b", string=search_str):
        logging.warning(f"Crash {syzkaller_id}: No '{search_str}' found")
        return None

    # find the first <span class="mono"> element after <b>{search_str}</b>
    fix_bisection_commit_text = crash_html.find("b", string=search_str).find_next("span", class_="mono").text

    # find the first commit hash in the text
    regex = re.compile(r"commit ([0-9a-f]{40})", re.MULTILINE)
    fix_bisection_commit_match = re.search(regex, fix_bisection_commit_text)

    if fix_bisection_commit_match:
        return fix_bisection_commit_match.group(1)
    else:
        logging.warning(f"Crash {syzkaller_id}: No commit in {fix_bisection_commit_text}")
        return None


def extract_fixing_commits(syzkaller_id, crash_html):
    """
    Extract the fixing commits from the crash html (starting with the "Fix commit" string at the top of the page)
    :param syzkaller_id: syzkaller id of the crash
    :param crash_html: html page of the crash
    :return: list of fixing commits
    """
    fixing_commits = []

    fix_commit_text = crash_html.find("b", string="Fix commit:")
    if not fix_commit_text:
        logging.warning(f"Crash {syzkaller_id} has no fix commit")
        return fixing_commits

    # find all <span class="mono"> elements between <b>"Fix commit:"</b> and <b>"Patched on:"</b>
    # and put them in a list
    for span in fix_commit_text.find_next_siblings("span", class_="mono"):
        if span.find("a"):
            fixing_commits.append(span.find("a").get("href").split("=")[-1])
        if span.find_next_sibling().name != "span":
            # We stop when we reach the next <b> element
            break

    return fixing_commits


def fetch_bics(crashes):
    """
    Fetches the bics from the fixing commits
    :param crashes: crashes to fetch the bics from
    :return: crashes with the bics
    """
    repo_node = get_config_nodes_repo_dict()["kernel"]
    commit_utils = CommitUtils(False, repo_node, None)
    for crash in tqdm(crashes, desc="Fetching bics from commits"):
        crash["bics"] = []
        for commit in crash["fix-commits"]:
            commit = commit_utils.get_commit_by_sha(commit)
            if not commit:
                continue
            commit_message = commit.message
            if "Fixes: " in commit_message:
                bic = commit_message.split("Fixes: ")[1].split(" ")[0][0:12]
                crash["bics"].append(bic)
    return crashes


def write_crashes_to_dir(crashes):
    """
    Writes the crashes to the reproducers directory
    :param crashes: crashes to write
    """
    for crash in crashes:
        os.makedirs("reproducers", exist_ok=True)

        with open(os.path.join(DIST_DIR, f"{crash['Id']}.json"), "w") as f:
            json.dump(crash, f, indent=2)


def save_to_database():
    """
    Saves the crashes to the database which are in the ``reproducers`` directory.
    If the crash is already in the database,
    it will be skipped.
    """
    syzkaller_crashes = [x for x in os.listdir(DIST_DIR) if x.endswith(".json")]
    repo_node = get_config_nodes_repo_dict()["kernel"]
    commit_utils = CommitUtils(True, repo_node, "kernel")
    db_repo = DBRepository()

    for crash in tqdm(syzkaller_crashes, desc="Saving to database"):
        fixing_commits = []
        bics = []
        with open(os.path.join(DIST_DIR, crash), "r") as f:
            crash = json.load(f)
            db_repo.save_syzkaller_info(crash)
            save_syzkaller_bisection(commit_utils, db_repo, crash)
            try:
                for fix in crash["fix-commits"]:
                    fixing_commits.append(commit_utils.get_commit_by_sha(fix))
                for bic in crash["bics"]:
                    bics.append(commit_utils.get_commit_by_sha(bic))

                fixing_commits = [x for x in fixing_commits if x is not None]
                bics = [x for x in bics if x is not None]

                # Since we might have multiple fixing commits and bics, we need to save all combinations of them
                commit_combinations = list(
                    product(fixing_commits if len(fixing_commits) > 0 else [None], bics if len(bics) > 0 else [None])
                )

                for fixing_commit, bic in commit_combinations:
                    db_repo.save_vcc_fixing_commit(
                        None,
                        fixing_commit.hexsha if fixing_commit else None,
                        bic.hexsha if bic else None,
                        "kernel",
                        "Syzkaller",
                        crash["Id"],
                    )
            except Exception as e:
                logging.warning(f"Failed to save crash {crash['Id']} - {e}")
                continue


def update_database_with_cve_ids():
    """
    The overarching goal is to connect syzkaller ids to cve ids.

    This is done by getting all cve_ids grouped by fixing commits and then getting all syzkaller ids
    that are connected to the fixing commit.
    We only want to save syzkaller ids which are connected to a single cve id.
    If a syzkaller id is connected to multiple cve ids, we don't save it.
    """
    db_repo = DBRepository()
    results = db_repo.get_all_syzkaller_ids_with_cve()
    for result in tqdm(results, desc="Updating database with CVE ids"):
        cve_ids = result["cve_ids"].split(",")
        if len(cve_ids) == 1:
            db_repo.save_cve_id_for_syzkaller_crash(cve_ids[0], result["fixing_sha"])


def save_syzkaller_bisection(commit_utils, db_repo, crash):
    """
    Saves the bisection commits of a crash to the database
    """
    # If the crash does not have bisection commits, we don't need to save it
    if not crash["fix-bisection-commit"] and not crash["cause-bisection-commit"]:
        return
    commit_utils.get_commit_by_sha(crash["fix-bisection-commit"])
    commit_utils.get_commit_by_sha(crash["cause-bisection-commit"])
    db_repo.save_syzkaller_bisect(crash)


def check_for_cves(commit: Commit):
    """
    Check if a commit message contains a CVE id
    :param commit: commit to check
    :return: CVE id if found, None otherwise
    """
    if not commit:
        return None
    commit_message = commit.message
    cve_regex = re.compile(r"CVE-\d{4}-\d{4,7}")
    if cve_regex.search(commit_message):
        cve_id = cve_regex.search(commit_message).group()
        return cve_id
    return None


if __name__ == "__main__":
    log_formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s]  %(message)s")
    root_logger = logging.getLogger()
    current_date = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    file_handler = logging.FileHandler(f"./logs/syzkaller_crawler_{current_date}.log")
    file_handler.setFormatter(log_formatter)
    root_logger.addHandler(logging.StreamHandler(sys.stdout))
    root_logger.addHandler(file_handler)
    main()
    save_to_database()
    update_database_with_cve_ids()
