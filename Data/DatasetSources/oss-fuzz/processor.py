import os
from multiprocessing import Queue, Process

from git import Commit

from Data.Database.db_repository import DBRepository
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import get_config_codes_list, get_config_nodes_repo_dict


def worker(queue_in):
    """Worker function for the multiprocessing pool."""
    while True:
        chunk = queue_in.get()
        if chunk is None:
            break
        run_worker(chunk)


def run_worker(chunk: list[dict]):
    """Runs the worker function."""
    print(f"PID {os.getpid()} started processing {len(chunk)} bugs...")
    db_repo = DBRepository()

    for bug in chunk:
        try:
            possible_commits = extract_relevant_commit(bug, False)
            for commit in possible_commits:
                db_repo.save_commit(commit, bug["project"])
                db_repo.save_vcc_fixing_commit(
                    None, commit.hexsha, None, bug["project"], "OSS_Fuzz", None, bug["local_id"]
                )
        except Exception as e:
            print(f"PID {os.getpid()} ({bug['local_id']}) Error: {e} ... skipping")

    print(f"PID {os.getpid()} finished processing {len(chunk)} bugs.")


def calculate_chunk_size(oss_fuzz_bugs: list[dict], num_processes: int) -> int:
    """Calculates the chunk size for the multiprocessing pool.
    Based on the implementation of multiprocessing.Pool
    """
    chunk_size, extra = divmod(len(oss_fuzz_bugs), num_processes * 4)
    if extra:
        chunk_size += 1
    return chunk_size


def extract_relevant_commit(
    oss_fuzz_entry: dict,
    search_regressed: bool,
) -> list[Commit]:
    """Extracts the relevant commit from a range of commits.

    Since OSS-Fuzz only provides a range of commits, we need to extract the relevant commits from the range.
    There can be multiple relevant commits, therefore, we return a list of commits.

    Extracting method adapted by Keller et al. (2023):
    `What Happens When We Fuzz? Investigating OSS-Fuzz Bug History <https://doi.org/10.48550/arXiv.2305.11433>`_
    :param oss_fuzz_entry: The OSS-Fuzz entry from the database containing all relevant information.
    :param search_regressed: A flag whether to search for the regressed commit or the fixed commit.
    :return: A list of relevant commits.
    :raises ValueError: If the config code is not found in the config.xml file.
    """
    if (config_code := oss_fuzz_entry.get("project")) not in get_config_codes_list():
        raise ValueError(f"Config code {config_code} not found in config.xml")

    repo_node = get_config_nodes_repo_dict()[config_code]
    commit_utils = CommitUtils(False, repo_node, None)

    start_commit = (
        oss_fuzz_entry.get("regressed_start_commit") if search_regressed else oss_fuzz_entry.get("fixed_start_commit")
    )

    end_commit = (
        oss_fuzz_entry.get("regressed_end_commit") if search_regressed else oss_fuzz_entry.get("fixed_end_commit")
    )

    if end_commit is None:
        # If no end commit is specified, we do not have a range of commits, therefore, we return the start commit.
        return [commit_utils.get_commit_by_sha(start_commit)]

    if start_commit == end_commit:
        # If the start and end commit are the same, we return the start commit.
        return [commit_utils.get_commit_by_sha(start_commit)]

    # Mention: The commit message mentions the bug ID of OSS-Fuzz
    # Sole Commit: The commit is the only commit in the range
    possible_commits = {"mention": [], "sole_commit": []}
    range_commits = commit_utils.get_range_of_commits(start_commit, end_commit)

    if len(range_commits) == 0:
        return []

    if len(range_commits) == 1:
        possible_commits["sole_commit"].append(range_commits[0])

    for commit in range_commits:
        if commit.message is None:
            continue
        if str(oss_fuzz_entry["local_id"]) in commit.message:
            possible_commits["mention"].append(commit)

    for key, value in possible_commits.items():
        if len(value) > 0:
            return value

    return []


def main():
    num_cores = os.cpu_count()

    db_repo = DBRepository()
    already_processed_local_ids = db_repo.get_all_entries_already_having_oss_fuzz_id()
    oss_fuzz_bugs = db_repo.get_all_oss_fuzz_bugs_with_relevant_projects(False, True)
    # Filter out already processed bugs
    oss_fuzz_bugs = [bug for bug in oss_fuzz_bugs if bug["local_id"] not in already_processed_local_ids]
    chunk_size = calculate_chunk_size(oss_fuzz_bugs, num_cores)

    print("Starting to process OSS-Fuzz bugs...")
    print("Total number of bugs:", len(oss_fuzz_bugs))

    queue_in = Queue()
    workers = []
    for _ in range(num_cores):
        worker_process = Process(target=worker, args=(queue_in,))
        worker_process.start()
        workers.append(worker_process)

    chunks = [oss_fuzz_bugs[i : i + chunk_size] for i in range(0, len(oss_fuzz_bugs), chunk_size)]
    for chunk in chunks:
        queue_in.put(chunk)

    for _ in range(num_cores):
        queue_in.put(None)

    for worker_process in workers:
        worker_process.join()

    print("Finished processing OSS-Fuzz bugs.")


if __name__ == "__main__":
    main()
