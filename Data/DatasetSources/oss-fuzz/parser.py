import re

from tqdm import tqdm

from Data.Database.constants import URL_REGEX, SHA_REGEX
from Data.Database.db_repository import DBRepository
from oss_fuzz_api import get_total_issues, get_all_issues, get_issue, get_comments, get_commit_range_urls

regressed_regex = rf"(?i)Regressed: ({URL_REGEX})"
crash_revision_regex = rf"(?i)Crash Revision: ({URL_REGEX})"
fixed_regex = rf"(?i)Fixed: ({URL_REGEX})"
verified_fixed_regex = rf"(?i)verified as fixed in ({URL_REGEX})"
csv_columns = [
    "localId",
    "summary",
    "status",
    "bugType",
    "project",
    "labelRefs",
    "regression_range_url",
    "fixed_range_url",
    "crash_type",
    "regressed_start_commit",
    "regressed_end_commit",
    "fixed_start_commit",
    "fixed_end_commit",
    "regressed_repo_url",
    "fixed_repo_url",
    "crash_state",
]


def _get_project_from_labels(label_refs):
    """
    Extract the project name from the labels of the issue.
    :param label_refs: List of label references
    :return: project name
    """
    for entry in label_refs:
        if "Proj-" in entry["label"]:
            return entry["label"].split("-")[1]
    return None


def parse_issue_info(issue):
    """
    We extract the following general information from the issue:

    - Summary
    - Id of the issue
    - Status
    - Bug type
    - Project name
    - Label
    - Status modified timestamp
    - Last modified timestamp
    :param issue: issue dict
    :return: dictionary with the extracted information
    """
    info = {
        "localId": issue.get("localId"),
        "summary": issue.get("summary"),
        "statusModifiedTimestamp": issue.get("statusModifiedTimestamp"),
        # In the OSS-Fuzz API, the last modified timestamp is called modifiedTimestamp hence the name
        "modifiedTimestamp": issue.get("modifiedTimestamp"),
        "status": None,
        "bugType": None,
        "project": None,
        "labelRefs": None,
    }
    if issue.get("statusRef"):
        info["status"] = issue["statusRef"].get("status")
    bug_types = []
    if issue.get("fieldValues"):
        for value in issue["fieldValues"]:
            bug_types.append(value["value"])
        info["bugType"] = ",".join(bug_types)
    if issue.get("labelRefs"):
        info["project"] = _get_project_from_labels(issue["labelRefs"])
        info["labelRefs"] = ",".join([x["label"] for x in issue["labelRefs"]])

    return info


def parse_list_of_all_issues():
    """
    Parses all issues from the oss-fuzz API.
    This function only saves general information which is available from the list of issues API call.
    :return: List of dictionaries with the extracted information
    """
    total_issues = get_total_issues()
    print("Total issues:", total_issues)
    issues = []

    for i in tqdm(range(0, total_issues, 1000), desc="Fetching issues in batches of 1000"):
        unparsed_issues = get_all_issues(i)
        for issue in unparsed_issues:
            issues.append(parse_issue_info(issue))

    return issues


def diff_list_of_all_issues():
    """
    Compare newly fetched data from the api with the latest existing list of issues.
    Only compare general information.
    :return: A dictionary with the following information:
    - updated_issues (updated timestamp)
    - old_issues (not updated)
    - new_issues (not existing in the old list)
    """
    db_repo = DBRepository()
    current_issues = db_repo.get_all_oss_fuzz_bugs()
    print("Total current issues in database:", len(current_issues))
    # We always fetch the latest issues from the API to compare them with the current issues in the database
    latest_issues_dict = parse_list_of_all_issues()

    # Create a dict with the local id as key and the issue as value for faster access
    current_issues_local_ids_dict = {issue["local_id"]: issue for issue in current_issues}

    updated_issues = []
    new_issues = []
    old_issues = []

    for latest_issue in latest_issues_dict:
        local_id = latest_issue["localId"]
        if local_id in current_issues_local_ids_dict:
            # Check if the issue was updated
            current_issue = current_issues_local_ids_dict[local_id]
            # The last modified timestamp might not be always set
            last_modified = current_issue["last_modified"] if current_issue["last_modified"] else 0
            if latest_issue["modifiedTimestamp"] > last_modified:
                # The issue was updated
                updated_issues.append(latest_issue)
            else:
                # The issue was not updated
                old_issues.append(latest_issue)
        else:
            # The issue is new if the local id is not in the old list
            new_issues.append(latest_issue)

    print("New issues:", len(new_issues))
    print("Updated issues:", len(updated_issues))
    print("Old issues:", len(old_issues))

    return {"updated_issues": updated_issues, "old_issues": old_issues, "new_issues": new_issues}


def parse_one_issue(local_id: int):
    """
    The parsed issue contains the following information:

    - Summary
    - Id of the issue
    - Status
    - Bug type
    - Project name
    - Label references
    - Crash_type
    - Regression_range
    - Fixed_range
    - Crash_state
    :param local_id: The local id of the issue
    :return: dictionary with the extracted information
    """
    parsed_issue = {}
    issue = get_issue(local_id)
    parsed_issue.update(parse_issue_info(issue))
    parsed_issue["regression_range_url"] = None
    parsed_issue["fixed_range_url"] = None
    parsed_issue["crash_type"] = None
    # These fields refer to the commit range in the repository if a valid commit range is found
    parsed_issue["regressed_start_commit"] = None
    parsed_issue["regressed_end_commit"] = None
    parsed_issue["fixed_start_commit"] = None
    parsed_issue["fixed_end_commit"] = None
    parsed_issue["regressed_repo_url"] = None
    parsed_issue["fixed_repo_url"] = None
    parsed_issue["crash_state"] = None

    comments = get_comments(local_id)
    for comment in comments:
        content = comment.get("content")
        if not content:
            continue

        regression_result = re.search(regressed_regex, content) or re.search(crash_revision_regex, content)
        fixed_result = re.search(fixed_regex, content) or re.search(verified_fixed_regex, content)
        crash_type_result = re.search(rf"(?i)Crash Type: (.*)", content)
        crash_state_result = re.search(rf"(?i)Crash State:\n(.*)", content, re.DOTALL)

        if crash_state_result:
            crash_results = crash_state_result.group(1)
            crash_results = crash_results.split("\n")
            temp = []
            for crash_result in crash_results:
                if crash_result.startswith("  "):
                    temp.append(crash_result.strip())
                else:
                    # We break if we reach the next section.
                    # This is the case if the next line does not start with two spaces
                    break
            # Remove empty strings
            temp = [x for x in temp if x]
            parsed_issue["crash_state"] = "--".join(temp)

        if regression_result:
            parsed_issue["regression_range_url"] = regression_result.group(1)
            if parsed_commit_range := parse_commit_range(parsed_issue["regression_range_url"], parsed_issue["project"]):
                parsed_issue["regressed_start_commit"] = parsed_commit_range["start"]
                parsed_issue["regressed_end_commit"] = parsed_commit_range["end"]
                parsed_issue["regressed_repo_url"] = parsed_commit_range["url"]

        if fixed_result:
            parsed_issue["fixed_range_url"] = fixed_result.group(1)
            if parsed_commit_range := parse_commit_range(parsed_issue["fixed_range_url"], parsed_issue["project"]):
                parsed_issue["fixed_start_commit"] = parsed_commit_range["start"]
                parsed_issue["fixed_end_commit"] = parsed_commit_range["end"]
                parsed_issue["fixed_repo_url"] = parsed_commit_range["url"]

        if crash_type_result:
            parsed_issue["crash_type"] = crash_type_result.group(1)

    return parsed_issue


def parse_commit_range(url: str, project_name: str):
    """
    Since there can be multiple commit ranges in the url provided by oss-fuzz,
    it is necessary to find the correct one for the project.
    :param url: The url provided by oss-fuzz
    :param project_name: The name of the project we are looking for
    :return: A dictionary with the start and end commit of the commit range.
    Also, the url of the commit range is provided if it is available.
    If the end commit is not specified, it is None.
    """
    commit_urls = get_commit_range_urls(url)
    for commit_url in commit_urls:
        if not commit_url.get("component") or commit_url["component"].casefold() != project_name.casefold():
            # Ignore commit ranges that are not for the project we are looking for
            continue

        commits = commit_url.get("link_text", "").split(":")

        for i, commit in enumerate(commits):
            if not re.search(rf"{SHA_REGEX}", commit):
                # If the commit is not a commit sha, we ignore it
                commits[i] = None

        if len(commits) == 1:
            # If the commit range is not specified, return the commit as start commit
            return {"start": commits[0], "end": None, "url": commit_url.get("link_url")}
        elif len(commits) == 2:
            # If the commit range is specified, return the start and end commit
            return {"start": commits[0], "end": commits[1], "url": commit_url.get("link_url")}

    return None


def get_all_local_ids(general_issues):
    """
    :return: A list of all local ids of the issues in the general_issues list
    """
    return [issue["localId"] for issue in general_issues]


def save_issues_to_db(diff_list):
    """
    Saves the issues to the database. Will take a while.
    :param diff_list: Dict of different categories of issues, according to the diff_list_of_all_issues function
    """
    db_repo = DBRepository()
    new_local_ids = get_all_local_ids(diff_list["new_issues"])
    updated_local_ids = get_all_local_ids(diff_list["updated_issues"])

    issues = []
    for local_id in tqdm(updated_local_ids, desc="Saving updated issues to database"):
        try:
            issue = parse_one_issue(local_id)
        except Exception as e:
            print(f"Could not parse issue with local id {local_id} due to: {e}")
            continue
        issues.append(issue)
        db_repo.update_oss_fuzz_bug(issue)

    for local_id in tqdm(new_local_ids, desc="Saving new issues to database"):
        try:
            issue = parse_one_issue(local_id)
        except Exception as e:
            print(f"Could not parse issue with local id {local_id} due to: {e}")
            continue
        issues.append(issue)
        db_repo.save_oss_fuzz_bug(issue)


if __name__ == "__main__":
    save_issues_to_db(diff_list_of_all_issues())
