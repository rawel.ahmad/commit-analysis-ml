"""
Contains the API calls for the OSS-Fuzz dataset.
"""
import base64
import json
import os
import re

import requests as req
from bs4 import BeautifulSoup
from ratelimit import sleep_and_retry, limits

from credentials import headers

LIST_ISSUES_URL = "https://bugs.chromium.org/prpc/monorail.Issues/ListIssues"
GET_ISSUE_URL = "https://bugs.chromium.org/prpc/monorail.Issues/GetIssue"
GET_COMMENTS_URL = "https://bugs.chromium.org/prpc/monorail.Issues/ListComments"


def _check_token():
    """
    Checks if the token is set.
    The token is either stored in the environment variable OSS_FUZZ_TOKEN or in credentials.py.
    The token in the environment variable has priority.
    :raises: Exception if the token is not set.
    """
    token = os.getenv("OSS_FUZZ_TOKEN") if os.getenv("OSS_FUZZ_TOKEN") else headers.get("X-Xsrf-Token")

    if not token or token == "TOKEN-HERE":
        raise Exception("Please set the token in the environment variable OSS_FUZZ_TOKEN or in credentials.py")
    headers["X-Xsrf-Token"] = token


def _get_issue_body(issue_id):
    """
    :param issue_id: The issue ID.
    :return: The body for the GetIssue API call.
    """
    return {
        "issueRef": {
            "localId": issue_id,
            "projectName": "oss-fuzz",
        }
    }


def _get_comments_body(issue_id):
    """
    :param issue_id:
    :return: The body for the ListComments API call.
    """
    return {
        "issueRef": {
            "localId": issue_id,
            "projectName": "oss-fuzz",
        }
    }


def _get_issues_body(start):
    """
    :param start: The start index (0-indexed).
    :return: The body for the ListIssues API call.
    """
    return {
        "cannedQuery": 1,
        "pagination": {
            "start": start,
            "maxItems": 1000,
        },
        "projectNames": ["oss-fuzz"],
        # We ignore issues that have the infra component since they are not related to a specific project
        # and are issues with the infrastructure of oss-fuzz
        "query": "status:Fixed,Verified Type=Bug,Bug-Security -component:Infra",
    }


@sleep_and_retry
@limits(calls=5, period=1)
def _post_call_api(url, body):
    """
    Makes a POST request to the API.
    The headers used are available in credentials.py.
    :param url: The URL to make the request to.
    :param body: The body of the request.
    :raises: Exception if the response status code is not 200.
    :return: The response text.
    """
    _check_token()
    response = req.post(url, headers=headers, json=body)
    if response.status_code != 200:
        raise Exception(f"Error: {response.status_code} {response.text}")
    return response.text


def get_issue(issue_id):
    response = _post_call_api(GET_ISSUE_URL, _get_issue_body(issue_id))
    # The API returns a malformed response, so we need to strip the first line
    response = response.lstrip(")]}'\n")
    json_data = json.loads(response).get("issue")
    return json_data


def get_comments(issue_id):
    response = _post_call_api(GET_COMMENTS_URL, _get_comments_body(issue_id))
    response = response.lstrip(")]}'\n")
    json_data: dict = json.loads(response)
    return json_data.get("comments", [])


def get_total_issues():
    response = _post_call_api(LIST_ISSUES_URL, _get_issues_body(0))
    response = response.lstrip(")]}'\n")
    json_data: dict = json.loads(response)
    return json_data.get("totalResults", 0)


def get_all_issues(start):
    """
    :param start: The start index (0-indexed).
    :return: A list of issues.
    """
    response = _post_call_api(LIST_ISSUES_URL, _get_issues_body(start))
    # The API returns a malformed response, so we need to strip the first line
    response = response.lstrip(")]}'\n")
    json_data: dict = json.loads(response)
    return json_data.get("issues", [])


def get_commit_range_urls(url: str) -> list[dict]:
    """
    Returns the commit range URLs from the OSS-Fuzz dataset.
    The page containing the commit range URLs is a single page application, so we need to parse the JavaScript code.
    The URLs are in a script tag that contains a base64 encoded string.
    This string is decoded and parsed as JSON.
    One entry in the list has the following attributes:

    - component: The name of the project.
    - link_text: The text of the link.
    - link_url: The link itself.
    :param url: The URL to the page containing the commit range URLs.
    :return: A list of commit range URLs.
    :raises: HTTPError if the request fails.
    """
    res = req.get(url)
    try:
        res.raise_for_status()
    except req.HTTPError as e:
        return []
    soup = BeautifulSoup(res.text, "html.parser")
    script_tags = soup.find_all("script")
    for script_tag in script_tags:
        if "document.querySelector('#page').info" in script_tag.text:
            base64_data_regex = r"atob\('([^']+)'\)"
            if match := re.search(base64_data_regex, script_tag.text):
                base64_data = match.group(1)
                data = base64.b64decode(base64_data).decode("utf-8")
                json_data = json.loads(data)
                return json_data.get("componentRevisionsList", [])
    return []
