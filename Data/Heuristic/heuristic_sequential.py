import json

from git import Repo
from tqdm import tqdm

from Data.Database.db_repository import DBRepository
from Data.Heuristic.Heuristics.LiPaxsonHeuristic import LiPaxsonHeuristic
from Data.Heuristic.Heuristics.VccfinderHeuristicSerial import VccfinderHeuristicSerial
from Data.Heuristic.Heuristics.VuldiggerHeuristic import VuldiggerHeuristic
from Data.Heuristic.Heuristics.VuldiggerHeuristic2 import VuldiggerHeuristic2
from Data.Utils.CVESearch import CVESearch
from Data.Utils.CommitUtils import CommitUtils
from Data.Utils.utils import (
    get_config_codes_list,
    calculate_confidence,
    get_config_nodes_repo_dict,
)


def main():
    selected_heuristic = "vuldigger2"
    db_repo = DBRepository()
    mappings = db_repo.get_mappings_without_vcc()
    config_codes = get_config_codes_list()

    for mapping in tqdm(mappings, desc=f"Running heuristic {selected_heuristic}"):
        if mapping["fixing_config_code"] not in config_codes:
            tqdm.write(f"Config code {mapping['fixing_config_code']} not found...skipping")
            continue
        heuristic, commit_utils, cve_search = init_heuristic(selected_heuristic, mapping["fixing_config_code"])
        cve = cve_search.get_cve_by_id(mapping["cve_id"])
        commit = commit_utils.get_commit_by_sha(mapping["fixing_sha"], True)
        if commit is None:
            tqdm.write(f"Commit {mapping['fixing_sha']} not found in {mapping['fixing_config_code']} ...skipping")
            continue
        results = heuristic.use_heuristic(commit, cve)
        if results:
            tqdm.write(f"Found {len(results)} possible VCCs for {mapping['fixing_sha']}")
            confidence = calculate_confidence(results, cve)
            result_json = {c.hexsha: i for c, i in results.items()}
            db_repo.update_heuristic_mapping(
                mapping["fixing_sha"],
                mapping["fixing_config_code"],
                json.dumps(result_json),
                confidence
            )
        else:
            # If no VCCs are found, we still want to update the mapping to indicate that we have checked it
            db_repo.update_heuristic_mapping(
                mapping["fixing_sha"],
                mapping["fixing_config_code"],
                json.dumps({}),
                False
            )


def init_heuristic(selected_heuristic, config_code):
    """
    Initializes the heuristic with the given name
    :param selected_heuristic: the name of the heuristic to initialize
    :param config_code: the config code to initialize the heuristic for
    :return: the initialized heuristic including the commit utils and the CVE search
    """
    repo_node = get_config_nodes_repo_dict()[config_code]
    repo_path = repo_node.find("./path").text
    repo = Repo(repo_path)
    commit_utils = CommitUtils(False, repo_node, config_code)
    cve_search = CVESearch(False, None)
    if selected_heuristic == "vuldigger2":
        heuristic = VuldiggerHeuristic2(repo, java=False)
    elif selected_heuristic == "vuldigger":
        heuristic = VuldiggerHeuristic(repo, java=False)
    elif selected_heuristic == "vccfinder":
        heuristic = VccfinderHeuristicSerial(repo, java=False)
    elif selected_heuristic == "lipaxson":
        heuristic = LiPaxsonHeuristic(repo, java=False)
    else:
        raise NotImplementedError(f"The heuristic {selected_heuristic} is currently not supported")

    return heuristic, commit_utils, cve_search


if __name__ == "__main__":
    main()
