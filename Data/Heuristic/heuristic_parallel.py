import json
import os
from multiprocessing import cpu_count, Queue, Process

from Data.Database.db_repository import DBRepository
from Data.Heuristic.heuristic_sequential import init_heuristic
from Data.Utils.utils import get_config_codes_list, calculate_confidence

selected_heuristic = "vuldigger2"


def run_heuristic(mapping_chunk):
    """
    Runs the heuristic on a chunk of mappings
    :param mapping_chunk: A list of mappings
    """
    print(f"PID {os.getpid()} Starting heuristic range")
    db_repo = DBRepository()
    config_codes = get_config_codes_list()
    for mapping in mapping_chunk:
        if mapping["fixing_config_code"] not in config_codes:
            # Since we already filtered in the main function, this should never happen
            print(f"PID {os.getpid()} ({mapping['fixing_config_code']}): Config code not found - skipping")
            continue
        heuristic, commit_utils, cve_search = init_heuristic(selected_heuristic, mapping["fixing_config_code"])
        cve = cve_search.get_cve_by_id(mapping["cve_id"])
        commit = commit_utils.get_commit_by_sha(mapping["fixing_sha"], True)
        if commit is None:
            print(
                f"PID {os.getpid()} ({mapping['fixing_config_code']}): Commit {mapping['fixing_sha']} not found - skipping"
            )
            continue
        results = heuristic.use_heuristic(commit, cve)
        if results:
            print(
                f"PID {os.getpid()} ({mapping['fixing_config_code']}): Found {len(results)} possible VCCs for {mapping['fixing_sha']}"
            )
            confidence = calculate_confidence(results, cve)
            result_json = {c.hexsha: i for c, i in results.items()}
            db_repo.update_heuristic_mapping(
                mapping["fixing_sha"],
                mapping["fixing_config_code"],
                json.dumps(result_json),
                confidence
            )
        else:
            print(f"PID {os.getpid()} ({mapping['fixing_config_code']}): No VCCs found for {mapping['fixing_sha']}")
            # If no VCCs are found, we still want to update the mapping to indicate that we have checked it
            db_repo.update_heuristic_mapping(
                mapping["fixing_sha"],
                mapping["fixing_config_code"],
                json.dumps({}),
                False
            )
    print(f"PID {os.getpid()} Finished heuristic range")


def worker(queue_in):
    while True:
        chunk = queue_in.get()
        if chunk is None:
            break
        run_heuristic(chunk)


def calculate_chunk_size(mappings, num_cores):
    """Uses the same implementation as multiprocessing.Pool to calculate chunk size"""
    chunk_size, extra = divmod(len(mappings), num_cores * 4)
    if extra:
        chunk_size += 1
    return chunk_size


def main():
    num_cores = cpu_count()

    db_repo = DBRepository()
    mappings = db_repo.get_mappings_without_vcc()
    config_codes = get_config_codes_list()
    # Filter out mappings that are not in the config codes
    mappings = [m for m in mappings if m["fixing_config_code"] in config_codes]
    chunk_size = calculate_chunk_size(mappings, num_cores)

    print(f"Total mappings: {len(mappings)}")
    print(f"Running heuristic {selected_heuristic} with {num_cores} cores with chunk size {chunk_size}")

    queue_in = Queue()
    workers = []
    for _ in range(num_cores):
        worker_process = Process(target=worker, args=(queue_in,))
        worker_process.start()
        workers.append(worker_process)

    chunks = [mappings[i: i + chunk_size] for i in range(0, len(mappings), chunk_size)]
    for chunk in chunks:
        queue_in.put(chunk)

    for _ in range(num_cores):
        queue_in.put(None)

    for worker_process in workers:
        worker_process.join()


if __name__ == "__main__":
    main()
