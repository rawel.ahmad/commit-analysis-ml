# Data and Configuration

This folder contains the separate sources used in the paper. The data is organized in the following way:

- `Database/`: Contains the database structure used in the paper and also an abstraction layer for the database. The
  database is a MySQL database.
- `Utils/`: Contains utility classes used.
- `Heuristic/`: Contains the heuristic used to map the VCCs to fixing commits. The heuristic is adapted from the work
  of Brack [1].
- `DatasetSources/`: Contains the dataset sources such as the ground truth data and the scripts used to generate it.
- `config.xml`: Contains the configuration used to create the mapping database. Read the section below for more
  information.

### Configuration

All information required to perform mappings is provided via a XML configuration file (`config.xml`).

#### Config codes

During all steps in any of the provided tools, different projects are identified using a unique config or product code.
Please make sure that these remain unique if you introduce new ones.
You can get a list of all config codes that have cve entries in the DB for by executing this SQL statement.

`SELECT DISTINCT(config_code) FROM vcc_mappings.cve_config_code`

#### XML structure

The config.xml is structured as follows:

Under the root node you can specify multiple `<product>` nodes each identified by the attribute `name` that is their
config code.

For each product you may specify multiple `<mapping>` nodes each representing one mapping approach. Keep in mind that
each node will trigger the mining process of the NVD and the repository anew.
A mapping node has to specify a `type` attribute. This attribute has to be one of the following:

1. **TypeCVEID:** The CVE-IDs of associated CVEs are extracted directly from the commit message
2. **TypeCommitSha:** The references of a CVE contain a link to the fixing commit from which the commit sha can be
   extracted
3. **TypeCommonID:** Both the CVE references and the commit messages contain common identifiers, like those of a
   bug/issue tracking system

##### Other mapping types

There are other mapping types that are used for mapping ground truth data to the database.
These are not used for mining and are third party mapping types.
These are the following:

- **LinuxKernelCVEs:** This type is used for mapping the LinuxKernelCVEs to the database.
- **PiantadosiApache:** This type is used for mapping the Piantadosi et al. dataset to the database.
- **VulnerabilityHistoryProject:** This type is used for mapping the Vulnerability History Project dataset to the
  database.

The `<mapping>` node requires at least one `<nvd>` and `<repo>` node
specifying which CVEs to look into and the source code location.
Both can have (may depend on the mapping type) a `<regex-list>` node
that contains regular expressions to extract the necessary information from the CVE references and commit messages.
Take a look at the `config.xml` provided for examples.

#### Provided configuration

The provided `config.xml` contains the configuration used to create major part of the mapping database.
Please note that only the mapping approaches for the projects referred to in the paper have been empirically
evaluated.

This configuration may contain some preliminary mapping approaches for the other projects that may lead to incorrect
mappings.

[1]: https://github.com/manuelbrack/VulnerabilityLifetimes