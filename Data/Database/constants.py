MYSQL_CONFIG = {"user": "root", "password": "toor", "host": "mysql", "database": "vcc_mappings"}
MONGO_CONFIG = {"host": "mongo", "port": 27017}
SHA_REGEX = r"[a-fA-F0-9]{40}"
# Reference: https://www.geeksforgeeks.org/python-check-url-string/
URL_REGEX = r"\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
# Not exhaustive
FIXED_KEYWORDS = r"fix(es|ed|up)?|mitigated|patch(ed|es)?"
# Not exhaustive
VULNERABLE_KEYWORDS = r"vulnerability|vulnerable|introduce(s|ed)?|exploitable( after)?"
