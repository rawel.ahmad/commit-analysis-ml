CREATE TABLE `vcc_mappings`.`syzkaller`
(
    `syzkaller_id`  varchar(50) NOT NULL,
    `title`         varchar(100) DEFAULT NULL,
    `syzkaller_url` varchar(100) DEFAULT NULL,
    `repro`         varchar(50)  DEFAULT NULL,
    `cause_bisect`  varchar(50)  DEFAULT NULL,
    `fix_bisect`    varchar(50)  DEFAULT NULL,
    `count`         int          DEFAULT NULL,
    `last`          varchar(50)  DEFAULT NULL,
    `reported`      varchar(50)  DEFAULT NULL,
    `patched`       varchar(50)  DEFAULT NULL,
    `closed`        varchar(50)  DEFAULT NULL,
    `patch`         varchar(255) DEFAULT NULL,
    PRIMARY KEY (`syzkaller_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = `utf8mb4`
  COLLATE = `utf8mb4_0900_ai_ci`;