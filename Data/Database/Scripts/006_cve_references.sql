CREATE TABLE `vcc_mappings`.`cve_references`
(
    `cve_id`    varchar(50)  DEFAULT NULL,
    `reference` varchar(512) DEFAULT NULL,
    KEY `cve_id` (`cve_id`),
    CONSTRAINT `FK_cve_references_cve` FOREIGN KEY (`cve_id`) REFERENCES `cve` (`cve_id`)
        ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = `utf8mb4`
  COLLATE = `utf8mb4_0900_ai_ci`;
