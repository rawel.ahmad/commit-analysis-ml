CREATE TABLE `vcc_mappings`.`link_fixing_commit_vcc`
(
    `mapping_id`              int         NOT NULL AUTO_INCREMENT,
    `fixing_sha`              varchar(50) NOT NULL,
    `fixing_config_code`      varchar(50) NOT NULL,
    `vcc_sha`                 varchar(50) NULL,
    `vcc_config_code`         varchar(50) NULL,
    `cve_id`                  varchar(50) NULL,
    `syzkaller_id`            varchar(50) NULL,
    `ossfuzz_id`              int         NULL,
    `mapping_type`            varchar(50) NOT NULL,
    `determined_by_heuristic` boolean     NOT NULL,
    `confidence_value`        boolean     NULL,
    `heuristic_mapping`       JSON        NULL,
    PRIMARY KEY (`mapping_id`),
    CONSTRAINT `FK_link_fixing_commit_vcc_commit_1`
        FOREIGN KEY (`fixing_sha`, `fixing_config_code`) REFERENCES `commit` (`com_sha`, `com_config_code`),
    CONSTRAINT `FK_link_fixing_commit_vcc_commit_2`
        FOREIGN KEY (`vcc_sha`, `vcc_config_code`) REFERENCES `commit` (`com_sha`, `com_config_code`),
    CONSTRAINT `FK_link_fixing_commit_vcc_cve`
        FOREIGN KEY (`cve_id`) REFERENCES `cve` (`cve_id`),
    CONSTRAINT `FK_link_fixing_commit_vcc_mapping_types`
        FOREIGN KEY (`mapping_type`) REFERENCES `ref_mapping_types` (`id`),
    CONSTRAINT `FK_link_fixing_commit_vcc_syzkaller`
        FOREIGN KEY (`syzkaller_id`) REFERENCES `syzkaller` (`syzkaller_id`),
    CONSTRAINT `FK_link_fixing_commit_vcc_oss_fuzz`
        FOREIGN KEY (`ossfuzz_id`) REFERENCES `oss_fuzz` (`local_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = `utf8mb4`
  COLLATE = `utf8mb4_0900_ai_ci`;