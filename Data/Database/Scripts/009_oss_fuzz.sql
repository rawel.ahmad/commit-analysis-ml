CREATE TABLE `vcc_mappings`.`oss_fuzz`
(
    `local_id`               int NOT NULL,
    `summary`                varchar(500)  DEFAULT NULL,
    `status`                 varchar(25)   DEFAULT NULL,
    `bug_type`               varchar(25)   DEFAULT NULL,
    `project`                varchar(25)   DEFAULT NULL,
    `labelRefs`              varchar(255)  DEFAULT NULL,
    `regression_range_url`   varchar(255)  DEFAULT NULL,
    `fixed_range_url`        varchar(255)  DEFAULT NULL,
    `crash_type`             varchar(500)  DEFAULT NULL,
    `regressed_start_commit` varchar(50)   DEFAULT NULL,
    `regressed_end_commit`   varchar(50)   DEFAULT NULL,
    `fixed_start_commit`     varchar(50)   DEFAULT NULL,
    `fixed_end_commit`       varchar(50)   DEFAULT NULL,
    `regressed_repo_url`     varchar(255)  DEFAULT NULL,
    `fixed_repo_url`         varchar(255)  DEFAULT NULL,
    `status_modified`        int           DEFAULT NULL,
    `last_modified`          int           DEFAULT NULL,
    `crash_state`            varchar(1000) DEFAULT NULL,
    PRIMARY KEY (`local_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = `utf8mb4`
  COLLATE = `utf8mb4_0900_ai_ci`;