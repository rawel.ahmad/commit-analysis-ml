CREATE TABLE `vcc_mappings`.`ref_mapping_types`
(
    `id`                  varchar(50) NOT NULL,
    `description_english` varchar(250) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = `utf8mb4`
  COLLATE = `utf8mb4_0900_ai_ci`;

INSERT IGNORE INTO `vcc_mappings`.`ref_mapping_types`(`id`, `description_english`)
VALUES ('', 'Empty Entry'),
       ('LinuxKernelCVEs',
        'Mappings as curated from the linux kernel cves project (https://github.com/nluedtke/linux_kernel_cves/tree/master/data)'),
       ('DebianSecurityTracker',
        'Mappings from the Debian security tracker project (https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/CVE/list)'),
       ('VulnerabilityHistoryProject_manual',
        'manually curated mappings from the vulnerability history project (https://github.com/VulnerabilityHistoryProject)'),
       ('VulnerabilityHistoryProject_automatic',
        'automatically curated mappings from the vulnerability history project (https://github.com/VulnerabilityHistoryProject)'),
       ('PiantadosiApache',
        'Mappings from Piantadosi et al. paper Fixing of Security Vulnerabilities in Open Source Projects: A Case Study of Apache HTTP Server and Apache Tomcat'),
       ('TypeCommonID', 'Mapping obtained by linking a common identifier (e.g. bug ID)'),
       ('TypeCommitSha', 'Commit identifier (sha) referenced in CVE entry'),
       ('TypeCVEID', 'CVE ID referenced in Commit message'),
       ('Syzkaller', 'Mappings from the syzkaller project'),
       ('OSS_Fuzz', 'Mappings from OSS-Fuzz')