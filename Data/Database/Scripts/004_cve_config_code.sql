CREATE TABLE `vcc_mappings`.`cve_config_code`
(
    `cve_id`      varchar(50)  DEFAULT NULL,
    `config_code` varchar(256) DEFAULT NULL,
    KEY `cve_id` (`cve_id`),
    CONSTRAINT `FK_cve_config_code_cve` FOREIGN KEY (`cve_id`) REFERENCES `cve` (`cve_id`)
        ON DELETE CASCADE,
    UNIQUE (`cve_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = `utf8mb4`
  COLLATE = `utf8mb4_0900_ai_ci`;
