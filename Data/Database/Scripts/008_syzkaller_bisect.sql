CREATE TABLE `vcc_mappings`.`syzkaller_bisect`
(
    `syzkaller_id`        varchar(50) NOT NULL,
    `cause_bisect_commit` varchar(50) NULL,
    `fix_bisect_commit`   varchar(50) NULL,
    CONSTRAINT `FK_syzkaller_bisect_syzkaller` FOREIGN KEY (`syzkaller_id`) REFERENCES `syzkaller` (`syzkaller_id`),
    CONSTRAINT `FK_syzkaller_bisect_cause_bisect_commit` FOREIGN KEY (`cause_bisect_commit`) REFERENCES `commit` (`com_sha`),
    CONSTRAINT `FK_syzkaller_bisect_fix_bisect_commit` FOREIGN KEY (`fix_bisect_commit`) REFERENCES `commit` (`com_sha`)
) ENGINE = InnoDB
  DEFAULT CHARSET = `utf8mb4`
  COLLATE = `utf8mb4_0900_ai_ci`;