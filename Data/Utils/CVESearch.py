import re
from xml.etree.ElementTree import Element

from pymongo import MongoClient
from tqdm import tqdm

from Data.Database.constants import MONGO_CONFIG
from Data.Database.db_repository import DBRepository
from Data.Utils.utils import extract_id


class CVESearch:
    """
    This class is used to fetch CVEs from the cve-search mongo db.
    """
    def __init__(self, db: bool, config_code: str | None):
        """
        Constructor
        :param db: Specifies if matching CVEs should be added to the DB
        :param config_code: Config code of the working project
        """
        self.db = db
        self.config_code = config_code

        if self.db and not config_code:
            raise Exception("config_code must be set if db flag is set")

        self.mongo_client = MongoClient(**MONGO_CONFIG)

        db_names = self.mongo_client.list_database_names()
        # cve-search stores cves in the db 'cvedb' and collection 'cves'
        if "cvedb" not in db_names:
            raise Exception('Mongo DB "cvedb" not found. Make sure that cve-search is running on your machine')

        if self.db:
            self.db_repo = DBRepository()

    def get_cve_by_id(self, cve_id: str) -> dict | None:
        """
        Fetches a CVE from the cve-search mongo db.
        Also save the CVE to the DB if the db flag is set.
        :param cve_id: The id of the CVE to fetch
        :return: the CVE as dict or None if not found
        """
        if cve_id is None:
            return None

        db = self.mongo_client.cvedb
        cve_collection = db.cves
        cve = cve_collection.find_one({"id": {"$regex": cve_id, "$options": "i"}})

        if self.db:
            self.db_repo.save_cve(cve, self.config_code)

        return cve

    def get_cves_by_cpe(self, cpe_regex: str) -> list[dict]:
        """
        Performs the fetching of cve entries from the cve-search mongo db
        It does not save the CVEs to the DB.
        :param cpe_regex: The regex to match the CPEs
        :return: list of CVEs
        """
        db = self.mongo_client.cvedb
        cve_collection = db.cves
        cves = cve_collection.find({"vulnerable_configuration": {"$regex": cpe_regex, "$options": "i"}})

        return list(cves)

    def get_map_from_common_id_to_cve(self, mapping_node: Element):
        """
        Fetches all CVEs that match the given regexes and extracts the bug ids from the references.
        The bug ids are then mapped to the CVEs.
        The result is a dict of bug ids to CVEs whose structure is:
        { bug_id: [cve_id1, cve_id2, ...], ... }
        :param mapping_node: The 'mapping' node from the config file
        :return: dict of mappings from common id to CVEs
        :raises Exception: If the mapping type is not 'TypeCommonID'
        """
        mapping_type = mapping_node.attrib["type"]
        if mapping_type != "TypeCommonID":
            raise Exception(f"Only Mapping type 'TypeCommonID' is supported. Got '{mapping_type}'")

        cve_mappings = {}
        unique_cves = set()
        cpe = mapping_node.find("./nvd/cpe").text
        print(f"Fetching CVEs for CPE '{cpe}'")
        cves = self.get_cves_by_cpe(cpe)

        for cve in tqdm(cves, desc="Extracting CVE mappings"):
            cve_id = cve["id"]

            # Iterate over all references and check if they match the regex
            for ref in cve["references"]:
                # Iterate over all regexes and check if the reference matches
                for regex_node in mapping_node.findall("./nvd/regex-list/regex"):
                    regex = regex_node.find("./contains").text
                    regex_res = re.search(regex, ref)
                    if regex_res:
                        unique_cves.add(cve_id)
                        # If the reference matches the regex extract the id
                        bug_id_list = extract_id(regex_node, ref)
                        for bug_id in bug_id_list:
                            if bug_id not in cve_mappings:
                                cve_mappings[bug_id] = []
                            cve_mappings[bug_id].append(cve_id)

        return cve_mappings
