import os
import re
from xml.etree.ElementTree import Element

from git import Commit, Repo
from tqdm import tqdm

from Data.Database.db_repository import DBRepository
from Data.Utils.utils import extract_id


class CommitUtils:
    """
    Utility class for commit related operations
    """

    def __init__(self, db: bool, repo_node: Element, config_code: str | None):
        """
        :param db: flag whether to write results to the database
        :param repo_node: repo node, must contain a path to the repository
        :param config_code: config code of the working project, where the commit belongs to
        """
        self.db = db
        self.repo_node = repo_node
        self.config_code = config_code
        self.repo_path = self.repo_node.find("./path").text

        if not os.path.exists(self.repo_path):
            raise OSError(f'Repository path "{self.repo_path}" not found!')

        self.repo = Repo(self.repo_path)

        if self.repo.bare:
            raise Exception(f'Found bare repository under "{self.repo_path}"!')

        if self.db and not config_code:
            raise Exception("config_code must be set if db flag is set")

        if self.db:
            self.db_repo = DBRepository()

    def get_commit_by_sha(self, com_sha: str | None, try_fetching_from_remote: bool = True) -> Commit | None:
        """
        Gets a commit by its sha.
        If the db flag is set, the commit will be saved to the database iff it does exist
        If the commit does not exist but the db flag is set, only the sha will be saved to the database
        :param com_sha: sha of the commit to get
        :param try_fetching_from_remote: a flag whether to try fetching the commit from the remote
        :return: commit object or None if the commit does not exist
        """
        if not com_sha:
            return None

        try:
            commit = self.repo.commit(com_sha)
        except ValueError:
            try:
                if try_fetching_from_remote:
                    # Assume orphaned commit, try fetching from origin
                    self.repo.git.fetch("origin", com_sha)
                    commit = self.repo.commit(com_sha)
                else:
                    commit = None
            except Exception:
                # Does not exist or is not reachable
                commit = None
        except Exception:
            # In case of any error, return None
            commit = None

        if self.db:
            self.db_repo.save_commit(commit if commit else com_sha, self.config_code)

        return commit

    def map_common_ids_to_commits(self, common_id_list: dict):
        """
        Maps common ids to commit by searching for the common id in the commit message
        and using the cve id list to map the commit to the cve id
        The structure of the common_id_list is as follows:
        { common_id: [cve_id, cve_id, ...], ... }
        :param common_id_list: list of common ids
        :return: dictionary of cve ids mapped to commits
        """
        unique_mapped_commits = {}

        commits = list(self.repo.iter_commits())
        print(f"Loaded {len(commits)} commits")

        for commit in tqdm(commits, desc="Mapping common ids to commits"):
            for regex_node in self.repo_node.findall("./regex-list/regex"):
                regex = regex_node.find("./contains").text
                search = re.search(regex, commit.message)
                if search:
                    bug_id_list = extract_id(regex_node, search.group(0))

                    for bug_id in bug_id_list:
                        if bug_id in common_id_list:
                            cves = common_id_list[bug_id]
                            for cve_id in cves:
                                if cve_id not in unique_mapped_commits:
                                    unique_mapped_commits[cve_id] = []
                                unique_mapped_commits[cve_id].append(commit)

        return unique_mapped_commits

    def get_list_of_commits(self):
        return list(self.repo.iter_commits())

    def get_range_of_commits(self, start, end):
        """
        Gets a range of commits between two commits.\n
        Since the order of the commits is reversed, the commits are reversed again
        :param start: Start commit
        :param end: End commit
        :return: List of commits in chronological order
        """
        try:
            commits = list(self.repo.iter_commits(rev=f"{start}..{end}"))
            commits.reverse()
            return commits
        except Exception:
            # In case of any error, return an empty list
            return []

    def get_commits_between_years(self, start_year, end_year):
        """
        Gets a range of commits between two years.\n
        Since the order of the commits is reversed, the commits are reversed again
        :param start_year: Start year
        :param end_year: End year
        :return: List of commits in chronological order
        """
        try:
            commits = list(self.repo.iter_commits(since=f"{start_year}-01-01", until=f"{end_year}-12-31"))
            return commits
        except Exception:
            # In case of any error, return an empty list
            return []
