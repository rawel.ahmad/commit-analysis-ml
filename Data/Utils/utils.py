import re
import warnings
from typing import List
from xml.etree.ElementTree import parse, Element

from Data import CONFIG_FILE_PATH
from Data.Database.constants import URL_REGEX, SHA_REGEX, VULNERABLE_KEYWORDS, FIXED_KEYWORDS


def contains_url(s: str):
    """
    Checks if the given string contains a URL
    :param s: the string to check
    :return: True if the string contains a URL, False otherwise
    """
    return bool(re.search(URL_REGEX, s))


def extract_url(s: str):
    """
    Extracts a URL from the given string
    :param s: the string to extract the URL from
    :return: the URL
    """
    return re.search(URL_REGEX, s).group()


def contains_commit_sha(s: str) -> bool:
    """
    Checks if the given string contains a commit SHA
    :param s: the string to check
    :return: True if the string contains a commit SHA, False otherwise
    """
    return bool(re.search(SHA_REGEX, s))


def extract_commit_sha(s: str):
    """
    Extracts a commit SHA from the given string
    :param s: the string to extract the commit SHA from
    :return: the commit SHA
    """
    return re.search(SHA_REGEX, s).group()


def contains_string(s1: str, s2: str) -> bool:
    """
    Checks if the given string s1 is in s2.
    :param s1: the string to check
    :param s2: the string to check against
    :return: True if s1 is in s2, False otherwise
    """
    return s1.casefold() in s2.casefold()


def is_vulnerable_commit(s: str):
    """
    Checks if the given string is a vulnerable commit
    The string might be vulnerable if it contains words such as "vulnerable", "vulnerability",
    "introduced vulnerability" etc.
    :param s: the string to check
    :return: True if the string is a vulnerable commit, False otherwise
    """
    return bool(re.search(VULNERABLE_KEYWORDS, s, re.IGNORECASE))


def is_fixing_commit(s: str):
    """
    Checks if the given string is a fixing commit
    The string might be fixing if it contains words such as "fix", "fixed", "fixes" etc.
    :param s: the string to check
    :return: True if the string is a fixing commit, False otherwise
    """
    return bool(re.search(FIXED_KEYWORDS, s, re.IGNORECASE))


def extract_repository_and_commit_id(url: str):
    """
    Extracts the name of the repository and the commit ID from a GitHub commit URL.
    Assumes that the URL is structured as follows:
    <https://github.com/{name_of_owner}/{repository_name}/commit/{commit_id}>
    :param url: str - GitHub commit URL
    :return: tuple - (repository, commit_id)
    """
    # Split the URL by '/'
    url_parts = url.split("/")

    # Check if the URL is valid
    if len(url_parts) != 7:
        raise ValueError(f"{url} is not a valid GitHub commit URL")

    # Extract the repository name and commit ID
    repository = f"{url_parts[3]}/{url_parts[4]}"
    commit_id = extract_commit_sha(url_parts[6])

    return repository, commit_id


def get_config_codes_list():
    """
    :return: a list of all the product codes in the config.xml file
    """
    xml_root = parse(CONFIG_FILE_PATH).getroot()
    return [p.attrib["name"] for p in xml_root.iter("product")]


def get_config_nodes_repo_dict():
    """
    :return: a dictionary mapping product codes to their repo node
    :raises ValueError: if no repo node is found for a product code
    """
    config_nodes = {}
    xml_root = parse(CONFIG_FILE_PATH).getroot()
    for p in xml_root.iter("product"):
        config_name = p.attrib["name"]
        repo_node = p.find("./mapping/repo")
        if repo_node is None:
            raise ValueError(f"No repo node found for config code {config_name}")
        config_nodes[config_name] = repo_node
    return config_nodes


def get_config_node(config_code: str) -> Element | None:
    """
    :param config_code: the config code to get the config node for
    :return: the config node for the given config code
    """
    if config_code not in get_config_codes_list():
        raise ValueError(f"Config code {config_code} not found in config.xml")
    xml_root = parse(CONFIG_FILE_PATH).getroot()
    return xml_root.find(f"./product[@name='{config_code}']")


def extract_id(regex_node: Element, s: str) -> List[str] | None:
    """
    Extracts the IDs from the given string using the given regex node
    :param regex_node: the regex node to use for extracting the IDs
    :param s: the string to extract the IDs from
    :return: a list of the extracted IDs or None if the regex node is invalid
    """
    id_type = regex_node.find("./id-extraction").attrib["type"]
    regex = regex_node.find("./id-extraction").text

    if id_type == "None":
        # Return the string as is
        return [s]

    if id_type == "Regex":
        # Extract the IDs using the regex
        return [m.group() for m in re.finditer(regex, s)]

    if id_type == "Cut":
        # Cut the string from the beginning to the first occurrence of the regex
        results = re.search(regex, s)
        if results:
            return [s.replace(results.group(), "")]
        else:
            return []

    warnings.warn(f"Invalid id-extraction type: {id_type}")
    return None


def calculate_confidence(results, cve) -> bool:
    """
    Calculates the confidence of the results as described in the paper by Wagner
    :param results: results of the heuristic
    :param cve: the CVE to check
    :return: True if the confidence is high, False otherwise
    """
    if cve and cve.get("cwe") and "264" in cve.get("cwe"):
        return False

    if len(results) < 2:
        # If there is only one commit, we can be sure
        return True

    blames_list = [blames for blames in results.values() if blames is not None]
    most_blamed = max(blames_list)
    second_most_blamed = sorted(blames_list)[-2]

    if most_blamed == second_most_blamed:
        # If two or more commits are blamed most often, we can't be sure
        return False

    # Calculated as described in the paper by Wagner
    confidence_threshold = (most_blamed - second_most_blamed) > sum(blames_list) / 3 and len(blames_list) < 5
    return confidence_threshold


def extract_most_blamed_heuristic(results):
    """
    Extracts the most blamed heuristic from the given results
    :param results: the results to extract the most blamed heuristic from
    :return: the most blamed heuristic
    """
    return max(results, key=results.get)
