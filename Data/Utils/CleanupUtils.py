from Data.Database.db_repository import DBRepository


def main():
    db_repo = DBRepository()
    db_repo.remove_initial_commits()


if __name__ == "__main__":
    main()
